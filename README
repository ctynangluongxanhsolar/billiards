Billiards 0.4.1

Copyright (C) 2005-2012 Dimitris Papavasileiou <dpapavas@gmail.com>

Introduction
============

Billiards is a free simulator for the game of billiards.  It can be
used to practice eightball, ninball and carom billiards on your
own or against your friends. It is also designed to be customizable
enough to allow one to tailor the simulator to his needs (to implement
new games for instance or to study the physics of the game by fiddling
with the physical properties of the equipment, setting up shots or
plotting ball trajectories, velocities and spin).

As a game Billiards is currently fairly stable and complete.  It
features most of the effects one encounters in a real billiards table.
You can apply english, knock balls off the table, perform masse shots
by elevating the cue and even miscue.  These features are not
`imitated' by, for example, artificially curving the path of the cue
ball when the cue is elevated.  They are a result of the physics
model employed by the simulator.  This leads me to believe that this
model is fairly accurate although I'd love to hear from more
experienced players on the subject.  The only flaw I can see in the
model so far is that it doesn't allow you to jump the cue ball by
elevating the cue and using bottom english.  This is due to the fact
that all objects in Billiards are considered completely rigid I
believe.  The elasticity of the cue tip should be taken into account
to get jump shots to work and elasticity may play an important role in
other cases like ball-cushion collisions as well.  I'm not sure if ODE
has enough support for flexible bodies to correctly implement such
effects though.  This needs to be further looked into.

Also be warned that billiards is designed to be a billiards simulator
not a `video game' in the usual sense.  Upon running it you get a
table and a cue and you can start playing straight away with the
default configuration and without having to get past a bunch of pesky
menus.  I expect that most people will use it for practice so that,
most of the time, one will play with the same configuration.  This
configuration should therefore be set once and for all (or once and
for long at least).  Some settings (the number of players, or game
type for example) will probably change more often and for these there
are command-line switches to set them on the fly when loading
Billiards.

You can, nevertheless, still use a GUI if you prefer.  It is probably
more convenient when experimenting with different values for the
various parameters or when changing options on the fly.  In these
cases you can load your favorite browser after you've started
billiards, connect to http://127.0.0.1:29176/ and use the pages you'll
find to set up new games or control the current game.  Alternatively
you can start Billiards through the command billiards-browser which
will start Techne's very own browser and point it to the correct url.

System Requirements
===================

In terms of hardware you'll generally need a GPU with decent OpenGL
support.  It should support GLSL shaders (the GL_ARB_shader_objects,
GL_ARB_vertex_shader and GL_ARB_shader_objects extensions) framebuffer
objects (GL_EXT_framebuffer_object) and vertex buffer objects
(GL_ARB_vertex_buffer_object).  These capabilities are common even
among cheap and relatively old GPUs so that I consider it reasonable
to use them as a bare minimum (alas, supporting old hardware through
multiple code paths is a nightmare and Billiards, or rather the system
it runs on, is already complicated enough for one person to handle).

There's also a plain-GL cel shaded codepath which doesn't use the GLSL
and framebuffer object extensions (and it can be made not to use
vertex buffer objects as well) and this is the only way to go for ATI
users I'm afraid (let me know if I'm mistaken).  If someone can help
in getting the GLSL shaders to run on ATI hardware let me know.  This
codepath could actually also be used to run the game on older hardware
although that'll require some changes to the code to get it to compile
on such systems.  I'll look into it if it gets requested by users.

Installation
============

Billiards is implemented on top of Techne, a general purpose,
programmable physical simulator and renderer so make sure you've
installed Techne before trying to install Billiards.  You can get it
here:

https://savannah.nongnu.org/projects/techne/

Other than that Billiards also requires the Lua 5.1 compiler.
 
The file INSTALL contains generic information on how to configure,
compile and install Billiards.  Generally after unpacking the tarball
and changing to the created directory you should enter:

./configure
make

as usual.  Then type:

sudo make install

to install the game.  If all went well you can run the game by typing:

billiards

at the command prompt.  If you get shader errors or segmentation faults
try:

billiards -Otoon instead.

Bugs
====

Please report any bugs (and you'll probably encounter lots of them) at
the project's home page at :
   
   http://savannah.nongnu.org/projects/billiards

License
=======

Billiards is released under the terms of the GNU General Public
License. See COPYING for details.

The manual is distributed under the terms of the GNU Free
Documentation License.  See the appendix for details.

All Lua scripts found in the scripts/imagery, scripts/meshes and
scripts/polyhedra directories and their subdirectories are image data
or geometry data and are distributed under the terms of the GNU Free
Documentation License.  See the appendix in the manual for details.
