-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "dynamics"
require "shapes"
require "physics"
require "widgets"
require "switches"
require "meshes"
require "textures"
require (options.toon and "toon" or "shading")

local theta, psi, alpha, beta = math.rad(125), 0, 0, 0

local function adjustcue(azimuth, elevation, horizontal, vertical)
   local c, d, r, l
   local n, t, t_0, psi_0
   local h, w, h, h_0, rcosc
   local gamma

   theta = tonumber(azimuth) or theta
   psi = tonumber(elevation) or psi
   alpha, beta = tonumber(horizontal) or alpha, tonumber(vertical) or beta

   if not bodies.observer.isaiming then
      return
   end

   -- Make sure we're aiming at the ball.

   gamma = math.length {alpha, beta, 0}
   if math.abs(gamma) >= 0.9 * billiards.ballradius then
      alpha = 0.9 * alpha / gamma * billiards.ballradius
      beta = 0.9 * beta / gamma * billiards.ballradius
   end

   -- Setup the cue rig.

   bodies.cue.orientation = transforms.relue (90 - math.deg(psi),
					      0,
					      math.deg(theta) + 90)

   c = transforms.fromnode (bodies.cue, {alpha, beta,
					 0.3 * billiards.cuelength +
					 billiards.ballradius + 
					 billiards.tipoffset})

   bodies.cue.position = math.add(bodies.cueball.position, c)

   joints.bridge.axis = transforms.fromnode (bodies.cue, {0, 0, 1})
   bodies.cue.bridge = joints.bridge

   -- Correct the elevation to clear the rails if necessary.

   w = billiards.tablewidth
   h = billiards.tableheight
   h_0 = billiards.cushionheight

   n = joints.bridge.axis
   d = transforms.fromnode (bodies.cue, {alpha, beta, 0})
   c = math.add(bodies.cueball.position, d)

   t = {
      (-c[1] - 0.5 * w) / n[1],
      (-c[1] + 0.5 * w) / n[1], 
      (-c[2] - 0.5 * h) / n[2],
      (-c[2] + 0.5 * h) / n[2]
   }

   for i = 1, 4 do
      if t[i] < 0 then
	 t[i] = math.huge
      end
   end

   t_0 = math.min (t[1], t[2], t[3], t[4], billiards.cuelength)
   psi_0 = math.atan2(h_0 + 0.02 - c[3], t_0)

   -- Take neighbouring balls into account as well.

   r = billiards.ballradius
   l = billiards.cuelength
   
   for _, ball in ipairs(bodies.balls) do
      c = ball.position
      d = physics.pointfrombody (bodies.cue,
				 {0, 0, -0.3 * billiards.cuelength})

      t_0 = n[1] * (c[1] - d[1]) + n[2] * (c[2] - d[2])
      s_0 = -n[1] * (c[2] - d[2]) + n[2] * (c[1] - d[1])

      if t_0 >= 0 and t_0 < l then
	 h = math.sqrt (r^2 - s_0^2 + 4e-4) + r - d[3]
	 psi_0 = math.max (psi_0, math.atan2(h, t_0))
      end
   end

   if psi_0 > psi then
      adjustcue(theta, psi_0, alpha, beta)
   else
      callhooks(billiards.adjusting)
   end
end

bodies.cue = bodies.capsule {
   length = 0.6 * billiards.cuelength,
   radius = billiards.cueradius,
   mass =  {billiards.cuemass,
	    {0, 0, 0},
	    {billiards.cueinertia, 0, 0,
	     0, billiards.cueinertia, 0,
	     0, 0, 9.13e-6}},

   link = function (self)
	     self.spin = {0, 0, 0}
	     self.velocity = {0, 0, 0}
	  end,

   butt = switches.button {
      surface = options.toon and toon.cel {
	 color = {1.0000, 0.9605, 0.6179},
	 mesh = resources.static "billiards/meshes/butt.lc" {
	    position = {0, 0, -0.36},
	 }
      } or shading.anisotropic {
	 diffuse = textures.clamped(resources.dofile "billiards/imagery/diffuse/cue.lc"),
	 specular = {0.3, 0.3, 0.3},
	 parameter = 64,
      
	 mesh = resources.static "billiards/meshes/butt.lc" {
	    position = {0, 0, -0.36},
	 }
      },

      highlight = widgets.highlight {
	 position = {0, 0, -0.36},
	 
	 mesh = resources.static "billiards/meshes/butt.lc" {},

	 color = {0.7, 0.6, 0.1},
	 width = 4.0,

	 thickness = 1.5,
	 radius = 0.07,
	 angle = 40,

	 prepare = function (self)
		      self.text = string.format("<span font=\"Sans 10\" color=\"Khaki\">Elevation: </span><span font=\"Sans 10\" color=\"White\">% d\194\176</span>", math.deg(bodies.cue.elevation))
		   end
      },
      
      selected = frames.event {
	 link = function (self)
		   self.parent.highlight.speed = 4
		end,

	 unlink = function (self)
		     self.parent.highlight.speed = -2
		  end,

	 buttonpress = function (self, button, x, y)
	    self.zero = y

	    if button == bindings.strike then
	       joints.bridge.transform = function(self)
	          -- This makes sure the cue doesn't trail off
	          -- when the user stops moving the mouse.

	          self.motor = {0, billiards.cueforce}
	       end
	    end
	 end,

	 buttonrelease = function (self, button, x, y)
            joints.bridge.motor = {0, billiards.cueforce}
	    joints.bridge.transform = nil
	 end,

	 motion = function (self, button, x, y)
	    if button == bindings.strike then
	       assert (joints.bridge.transform ~= nil)

	       joints.bridge.motor = {-billiards.stroke * (y - self.zero),
				      billiards.cueforce}
	    elseif button == bindings.elevate then
	       self.ancestry[2].elevation = math.clamp(bodies.cue.elevation -
						       0.005 * (y - self.zero),
						       0, math.pi / 2) 
	    end

	    self.zero = y
	 end,
     },
   },

   tip = switches.button {
      surface = options.toon and toon.cel {
	 color = {0.9065, 0.9212, 0.9197},
	 mesh = resources.static "billiards/meshes/tip.lc" {
	    position = {0, 0, -0.41},
	 }
      } or shading.anisotropic {
	 diffuse = {0.9065, 0.9212, 0.9197},
	 specular = {0.3, 0.3, 0.3},
	 parameter = 64,
      
	 mesh = resources.static "billiards/meshes/tip.lc" {
	    position = {0, 0, -0.41},
	 }
      },

      highlight = widgets.highlight {
	 position = {0, 0, -0.41},

	 mesh = resources.static "billiards/meshes/tip.lc" {},
	 
	 color = {0.7, 0.6, 0.1},
	 width = 4.0,
	 thickness = 1.5,
	 radius = 0.07,
	 angle = 40,

	 prepare = function (self)
		      self.text = string.format("<span font=\"Sans 10\" color=\"Khaki\">Sidespin:</span>\t<span font=\"Sans 10\" color=\"White\">% d%%, </span><span font=\"Sans 10\" color=\"Khaki\">Follow:</span>\t<span font=\"Sans 10\" color=\"White\">% d%%</span>", bodies.cue.sidespin /
						billiards.ballradius * 100,
					     bodies.cue.follow /
						billiards.ballradius * 100)
		   end
      },

      selected = frames.event {
	 link = function (self)
		   self.parent.highlight.speed = 4
		end,

	 unlink = function (self)
		     self.parent.highlight.speed = -2
		  end,

	 buttonpress = function (self, button, x, y)
	    self.zero = {x, y}
	 end,

	 motion = function (self, button, x, y)
	    if button == bindings.offset then
	       local dx, dy

	       dx = x - self.zero[1]
	       dy = y - self.zero[2]

	       self.ancestry[2].sidespin = bodies.cue.sidespin + 0.0005 * dx
	       self.ancestry[2].follow = bodies.cue.follow - 0.0005 * dy
	    end

	    self.zero = {x, y}
	 end,
      }
   },

   iscue = true,
}

joints.bridge = joints.slider {
   motor = {0, billiards.cueforce},
   stops = {{-1/0, billiards.tipoffset + 0.1}, {0, 0}, 0}
}

local meta = getmetatable (bodies.cue)
local oldindex = meta.__index
local oldnewindex = meta.__newindex

meta.__index = function (self, key)
		  if key == "azimuth" then
		     return theta
		  elseif key == "elevation" then
		     return psi
		  elseif key == "sidespin" then
		     return alpha
		  elseif key == "follow" then
		     return beta
		  elseif key == "speed" then
		     return joints.bridge.state[2]
		  else
		     return oldindex (self, key)
		  end
	       end

meta.__newindex = function (self, key, value)
		     if key == "azimuth" then
			adjustcue (value, psi, alpha, beta)
		     elseif key == "elevation" then
			adjustcue (theta, value, alpha, beta)
		     elseif key == "sidespin" then
			adjustcue (theta, psi, value, beta)			
		     elseif key == "follow" then
			adjustcue (theta, psi, alpha, value)			
		     else
			oldnewindex(self, key, value)
		     end
		  end

meta.__tostring = function(self)
		     return "cue stick"
		  end

billiards.aiming.showcue = function ()
			     -- Link the cue when entering
			     -- aiming mode.

			      bodies.cue.parent = bodies.cueball.parent
			   end

billiards.looking.hidecue = function ()
			       -- Unlink the cue when entering
			       -- looking mode.

			       bodies.cue.parent = nil
			    end

billiards.waiting.hidecue = function ()
			       -- Unlink the cue when entering
			       -- waiting mode.

			       bodies.cue.parent = nil
			    end

billiards.cuecollision.releasecue = function ()
				       -- Release the grip on the cue to
				       -- allow it to rebound off the ball

				       bodies.cue.bridge = nil
				       joints.bridge.motor = {0, billiards.cueforce}
				    end
