-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "widgets"
require "morewidgets"

graph.message = widgets.display {
   padding = {0.03, 0.03},
   align = {-1, -1},
}

local meta = getmetatable (graph.message)
local oldindex = meta.__index
local oldnewindex = meta.__newindex

meta.__index = function (self, key)
		  if key == "text" then
		     return self.string and self.string.text
		  else
		     return oldindex (self, key)
		  end
	       end

meta.__newindex = function (self, key, value)
		     if key == "text" then
			self.string = widgets.layout {
			   align = {-1, 0},

			   text = string.format ([[
<span font="Sans 12" color="palegoldenrod">%s</span>]], tostring(value)),

			   fader = frames.timer {
			      period = 0.02,
			      timeout = 5,
				    
			      tick = function (self, n, delta, elapsed)
					self.parent.opacity = self.timeout -
					   elapsed
					      
					if elapsed >= self.timeout then
					   self.ancestry[2] = nil
					end
				     end
			   }
			}
		     else
			oldnewindex(self, key, value)
		     end
		  end
