-- Copyright 2008 Dimitris Papavasiliou

-- Permission is granted to copy, distribute and/or modify this document
-- under the terms of the GNU Free Documentation License, Version 1.2
-- or any later version published by the Free Software Foundation;
-- with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
-- Texts.  A copy of the license is included in the section entitled ``GNU
-- Free Documentation License''.

require "pack"

return {
    size = {4, 6};    vertices = pack.fromfloats "\0\0\32\65\0\0\32\65\0\0\0\0\0\0\0\0\0\0\0\0\0\0\128\63\121\13\182\65\162\32\182\65\0\0\32\65\0\0\32\193\0\0\0\0\0\0\0\0\0\0\0\0\0\0\128\63\121\13\182\65\150\64\174\193\0\0\32\193\0\0\32\193\0\0\0\0\0\0\0\0\0\0\0\0\0\0\128\63\187\83\174\193\150\64\174\193\0\0\32\193\0\0\32\65\0\0\0\0\0\0\0\0\0\0\0\0\0\0\128\63\187\83\174\193\162\32\182\65";
    indices = pack.fromunsignedshorts"\0\0\3\0\2\0\0\0\2\0\1\0";
}
