-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "shapes"

local w = billiards.tablewidth
local h = billiards.tableheight
local h_0 = billiards.cushionheight

-- The pool table.

return options.pool and bodies.environment {
   isenvironment = true,

   pockets = resources.polyhedron "billiards/polyhedra/pockets.lc" {
      ispocket = true,

      surface = options.toon and toon.cel {
	 color = {0.0087, 0.0074, 0.0083},
	 mesh = resources.scaled "billiards/meshes/pockets.lc" () 
      } or shading.lambert {
	 diffuse = {0.05, 0.05, 0.05},
	 specular = {0.4, 0.4, 0.4},
	 parameter = 16,
	 
	 mesh = resources.scaled "billiards/meshes/pockets.lc" (),
      }
   },

   slot = bodies.composite {
      bodies.box {
	 position = {0.5 * w + 0.05, 0, -0.19},
	 size = {0.2, h / 1.42 * 0.9, 0.04},
	 isreturn = true,
      },

      bodies.box {
	 position = {0.5 * w + 0.13, 0, -0.138},
	 size = {0.03, h / 1.42 * 0.9, 0.067},
	 isreturn = true,
      },

      bodies.box {
	 position = {0.5 * w - 0.04, 0, -0.087},
	 size = {0.04, h / 1.42 * 0.86, 0.169},
	 isreturn = true,
      },
   },

   frame = resources.polyhedron "billiards/polyhedra/pooltop.lc" {
      istable = true,

      wood = options.toon and toon.cel {
	 color = math.scale({0.2156, 0.0380, 0.0019}, 0.2),
	 mesh = resources.scaled "billiards/meshes/pooltable.lc" ()
      } or shading.phong {
	 diffuse = resources.mirrored "billiards/imagery/diffuse/bilinga.lc",
	 specular = resources.mirrored "billiards/imagery/specular/bilinga.lc",
	 parameter = 32,
	 
	 mesh = resources.scaled "billiards/meshes/pooltable.lc" (),
      },

      markers = options.toon and toon.cel {
	 color = {0.97, 0.97, 0.82},
	 mesh = resources.laidout "billiards/meshes/dots.lc" ()
      } or shading.phong {
	 position = {0, 0, 1e-4},
	 diffuse = {0.97, 0.87, 0.72},
	 specular = {0.45, 0.435 ,0.425},
	 parameter = 16,

	 mesh = resources.laidout "billiards/meshes/dots.lc" (),
      },

      panels = options.toon and toon.cel {
	 color = {0.8441, 0.8205, 0.3944},
	 mesh = resources.scaled "billiards/meshes/panels.lc" (),
      } or shading.anisotropic {
	 diffuse = resources.mirrored "billiards/imagery/diffuse/brushed.lc",
	 specular = resources.mirrored "billiards/imagery/specular/brushed.lc",
	 parameter = {64, 0},

-- 	 frames.timer {
-- 	    period = 0,
-- 	    tick = function (self, n, delta, elapsed)
-- 		      self.parent.parameter = {128, elapsed}
-- 		      print (elapsed)
-- 		   end
-- 	 },
	 
	 mesh = resources.scaled "billiards/meshes/panels.lc" (),
      }
   },

   cloth = bodies.composite {
      -- The bed.

      bodies.box {
	 position = {0, 0, -0.005},
	 size = {w, h, 0.01},
	 isbed = true,
      },

      -- The cushions.

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = 0.5 * w - 0.167,
	 position = {0.25 * w - 0.01025, 0.5 * h + 0.003, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = 0.5 * w - 0.167,
	 position = {- 0.25 * w + 0.01025, 0.5 * h + 0.003, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = 0.5 * w - 0.167,
	 position = {0.25 * w - 0.01025, -0.5 * h - 0.003, h_0},
	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (0, 90, 0),
	 radius = 0.002,
	 length = 0.5 * w - 0.167,
	 position = {- 0.25 * w + 0.01025, -0.5 * h - 0.003, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (90, 0, 0),
	 radius = 0.002,
	 length = h - 0.183,
	 position = {0.5 * w + 0.001, 0, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler (90, 0, 0),
	 radius = 0.002,
	 length = h - 0.183,
	 position = {-0.5 * w - 0.001, 0, h_0},

	 iscushion = true
      },

      -- The pocket jaws, Top.

      bodies.capsule {
	 orientation = transforms.euler(90, -57, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {0.5 * w - 0.069, 0.5 * h + 0.017, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler(90, 20, -5),
	 radius = 0.002,
	 length = 0.031,
	 position = {0.066, 0.5 * h + 0.017, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler(90, -20, -5),
	 radius = 0.002,
	 length = 0.031,
	 position = {-0.066, 0.5 * h + 0.017, h_0},

	 iscushion = true
      },
      
      bodies.capsule {
	 orientation = transforms.euler(90, 57, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {-0.5 * w + 0.069, 0.5 * h + 0.017, h_0},

	 iscushion = true
      },

      -- Bottom.

      bodies.capsule {
	 orientation = transforms.euler(90, 57, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {0.5 * w - 0.069, -0.5 * h - 0.017, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler(90, -20, -5),
	 radius = 0.002,
	 length = 0.031,
	 position = {0.066, -0.5 * h - 0.017, h_0},

	 iscushion = true
      },

      bodies.capsule {
	 orientation = transforms.euler(90, 20, -5),
	 radius = 0.002,
	 length = 0.031,
	 position = {-0.066, -0.5 * h - 0.017, h_0},

	 iscushion = true
      },
      
      bodies.capsule {
	 orientation = transforms.euler(90, -57, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {-0.5 * w + 0.069, -0.5 * h - 0.017, h_0},

	 iscushion = true
      },

      -- Left.

      bodies.capsule {
	 orientation = transforms.euler(90, 33, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {0.5 * w + 0.015, -0.5 * h + 0.067, h_0},

	 iscushion = true
      },


      bodies.capsule {
	 orientation = transforms.euler(90, -33, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {0.5 * w + 0.015, 0.5 * h - 0.067, h_0},

	 iscushion = true
      },

      -- Right.

      bodies.capsule {
	 orientation = transforms.euler(90, -33, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {-0.5 * w - 0.015, -0.5 * h + 0.067, h_0},

	 iscushion = true
      },


      bodies.capsule {
	 orientation = transforms.euler(90, 33, -5),
	 radius = 0.002,
	 length = 0.053,
	 position = {-0.5 * w - 0.015, 0.5 * h - 0.067, h_0},

	 iscushion = true
      },
      
      -- The cloth surface.

      surface = options.toon and toon.cel {
	 color = {0.3194, 0.6223, 0.0766},
	 mesh = resources.scaled "billiards/meshes/poolcloth.lc" ()
      } or shading.oren {
	 diffuse = resources.periodic "billiards/imagery/diffuse/greencloth.lc",
	 parameter = 3.2,
	 
	 mesh = resources.scaled "billiards/meshes/poolcloth.lc" ()
      },
   },
}