-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- Calculate an opening position based on the chosen
-- game if no opening has been specified excplicitly.

require "morewidgets"

if not billiards.opening then
   if options.eightball then
      local left, right, isplaced

      left, right = math.random (3, 8), math.random (10, 16)
      isplaced = {[2] = true, [9] = true, [left] = true, [right] = true}

      -- Set up the balls for eight-ball.

      billiards.opening = {
	 {0.25 * billiards.tablewidth, 0, billiards.ballradius}
      }

      for j = 1, 5 do
	 for i = 1, j do
	    local k, n

	    k = j * (j - 1) / 2 + i

	    if k == 1 then
	       n = 2
	    elseif k == 5 then
	       n = 9
	    elseif k == 11 then
	       n = left
	    elseif k == 15 then
	       n = right
	    else
	       repeat
		  n = math.random (3, 16)
	       until not isplaced[n]
	    end

	    isplaced[n] = true
	    
	    billiards.opening[n] = {
		  -0.25 * billiards.tablewidth -
		  1.005 * billiards.ballradius * j * math.sqrt(3) +
	          0.005 * billiards.ballradius * (math.random() - 0.5),
	          1.005 * billiards.ballradius * (2 * i - j - 1) +
		  0.005 * billiards.ballradius * (math.random() - 0.5),
		  billiards.ballradius
	    }
	 end
      end
   elseif options.nineball then
      local isplaced

      -- Set up the balls for nine-ball.

      isplaced = {[2] = true, [10] = true}

      billiards.opening = {
	 {0.25 * billiards.tablewidth, 0, billiards.ballradius}
      }

      for j = 1, 5 do
	 for i = 1, 3 - math.abs(j - 3) do
	    local k, n

	    k = j < 5 and j * (j - 1) / 2 + i or 9

	    if k == 1 then
	       n = 2
	    elseif k == 5 then
	       n = 10
	    else
	       repeat
		  n = math.random (3, 9)
	       until not isplaced[n]
	    end

	    isplaced[n] = true
	    
	    billiards.opening[n] = 
	       {-0.25 * billiards.tablewidth -
		1.005 * billiards.ballradius * j * math.sqrt(3) +
	        0.002 * billiards.ballradius * (math.random() - 0.5),
	        1.005 * billiards.ballradius * (2 * i + math.abs(j - 3) - 4) +
	        0.002 * billiards.ballradius * (math.random() - 0.5),
	        billiards.ballradius}
	 end
      end
   elseif options.carom then
      -- Set up the balls for carom.

      billiards.opening = {
	 {0.25 * billiards.tablewidth, 0.1524, billiards.ballradius},
	 {0.25 * billiards.tablewidth, 0, billiards.ballradius},
	 {-0.25 * billiards.tablewidth, 0, billiards.ballradius}
      }
   elseif options.experiment then
      billiards.opening = {
	 {0.25 * billiards.tablewidth, 0, billiards.ballradius}
      }
   end   
end
require "moremeshes"
require "resources"
require (options.toon and "toon" or "shading")

require "textures"
require "switches"
require "moremath"
require "shapes"

local function respotball (ball, offset)
   if not offset then 
      offset = 0.25 * billiards.tablewidth
   end

   ball.position = {offset, 0, billiards.ballradius}
   ball.velocity = {0, 0, 0}
   ball.spin = {0, 0, 0}

   for _, other in pairs (bodies.balls) do
      local p = ball.position
      local q = other.position
      local r = billiards.ballradius

      if math.distance (p, q) <= 2 * r and other ~= ball then
   	 if options.pool then
   	    respotball (ball, q[1] - math.sqrt (4 * r * r - q[2] * q[2]) - 1e-3)
   	 else
   	    respotball (ball, offset > 0 and -offset or 0)
   	 end
	 
   	 break
      end
   end
end

if options.experiment then
   -- Choose ball decals if none have been specified.

   if not billiards.decals then
      billiards.decals = {
	 {0.97, 0.97, 0.82}
      }
   end
else
   if options.eightball then
      billiards.decals = {
	 {0.97, 0.97, 0.82},
	 resources.patched "billiards/imagery/diffuse/one.lc",
	 resources.patched "billiards/imagery/diffuse/two.lc",
	 resources.patched "billiards/imagery/diffuse/three.lc",
	 resources.patched "billiards/imagery/diffuse/four.lc",
	 resources.patched "billiards/imagery/diffuse/five.lc",
	 resources.patched "billiards/imagery/diffuse/six.lc",
	 resources.patched "billiards/imagery/diffuse/seven.lc",
	 resources.patched "billiards/imagery/diffuse/eight.lc",
	 resources.patched "billiards/imagery/diffuse/nine.lc",
	 resources.patched "billiards/imagery/diffuse/ten.lc",
	 resources.patched "billiards/imagery/diffuse/eleven.lc",
	 resources.patched "billiards/imagery/diffuse/twelve.lc",
	 resources.patched "billiards/imagery/diffuse/thirteen.lc",
	 resources.patched "billiards/imagery/diffuse/fourteen.lc",
	 resources.patched "billiards/imagery/diffuse/fifteen.lc",
      }
   elseif options.nineball then
      billiards.decals = {
	 {0.97, 0.97, 0.82},
	 resources.patched "billiards/imagery/diffuse/one.lc",
	 resources.patched "billiards/imagery/diffuse/two.lc",
	 resources.patched "billiards/imagery/diffuse/three.lc",
	 resources.patched "billiards/imagery/diffuse/four.lc",
	 resources.patched "billiards/imagery/diffuse/five.lc",
	 resources.patched "billiards/imagery/diffuse/six.lc",
	 resources.patched "billiards/imagery/diffuse/seven.lc",
	 resources.patched "billiards/imagery/diffuse/eight.lc",
	 resources.patched "billiards/imagery/diffuse/nine.lc",
      }
   elseif options.carom then
      billiards.decals = {
	 {0.97, 0.97, 0.82},
	 {0.92, 0.66, 0.18},
	 {0.4, 0.12, 0.1},
      }
   end
end

meshes.ball = function ()
   local mesh = meshes.sphere (billiards.ballradius, 32, 16)
		 
   for i = 7, #mesh.vertices, 8 do
      mesh.vertices[i] = mesh.vertices[i] * 4 - 1.5
      mesh.vertices[i + 1] = mesh.vertices[i + 1] * 2 - 0.5
   end
   
   return meshes.static(mesh)
end

-- Create the balls.

bodies.balls = {}

for i = 1, #billiards.opening do
   bodies.balls[i] = bodies.ball {
      position = billiards.opening[i],

      index = i,

      surface = switches.button {
	 shading = options.toon and toon.cel {
	    color = billiards.decals[i],
	    mesh = meshes.ball()
	 } or shading.fresnel {
	    diffuse = billiards.decals[i],
	    specular = {0.7, 0.7, 0.7},
	    parameter = {96, 0.1},
	    
	    mesh = meshes.ball()
	 },

	 highlight = widgets.highlight {
	    color = {0.7, 0.6, 0.1},
	    opacity = 0.55,
	    width = 5.0,

	    mesh = meshes.ball()
	 },

	 selected = frames.event {
	    link = function (self)
		      self.parent.highlight.speed = 4
		   end,

	    unlink = function (self)
		      self.parent.highlight.speed = -2
		   end,

	    buttonpress = function (self, button, x, y)
			     self.zero = {x, y}

			     if button == bindings.move and
				self.ancestry[2].position[3] > 0 and
				not bodies.observer.isaiming then
				self.ancestry[2].home = self.ancestry[2].position
			     end

			     if button == bindings.ready then
				if self.ancestry[2].position[3] < 0 then
				   respotball(self.ancestry[2])
				elseif self.ancestry[2] == bodies.cueball then
				   if bodies.observer.islooking and not
				      (bodies.cueball.ispocketed or
				       bodies.cueball.isout) then
				      bodies.observer.isaiming = true
				   elseif bodies.observer.isaiming then
				      bodies.observer.islooking = true
				   end
				elseif bodies.observer.islooking then
				   bodies.cueball = self.ancestry[2]
				end

				bodies.observer.longitude = bodies.cueball.position[1]
				bodies.observer.latitude = bodies.cueball.position[2]
			     end
			  end,

	    buttonrelease = function (self, button)
			       if self.ancestry[2].home then
				  self.ancestry[2].home = nil
			       end
			    end,

	    motion = function (self, button, x, y)
			if self.ancestry[2].home then
			   local w, h, u, v, dx, dy
			   local eye

			   eye = bodies.observer.feet.legs.waist.torso.neck.eye

			   dx = x - self.zero[1]
			   dy = y - self.zero[2]
			   
			   w = billiards.tablewidth / 2 - billiards.ballradius - 1e-3
			   h = billiards.tableheight / 2 - billiards.ballradius - 1e-3

			   u = transforms.fromnode(eye, {0, 1, 0})
			   v = transforms.fromnode(eye, {0, 0, -1})

			   u = math.normalize(math.project(u, {0, 0, 1}))
			   v = math.normalize(math.project(v, {0, 0, 1}))

			   self.ancestry[2].home = {
			      math.clamp(self.ancestry[2].home[1] +
					 0.003 * (dx * u[1] - dy * v[1]),
				      -w, w),
			      math.clamp(self.ancestry[2].home[2] +
					 0.003 * (dx * u[2] - dy * v[2]),
				      -h, h),
			      billiards.ballradius
			   }

			   bodies.observer.longitude = self.ancestry[2].home[1]
			   bodies.observer.latitude = self.ancestry[2].home[2]
			end
			
			self.zero = {x, y}
		     end,
	 },
      },

      shadow = options.toon and frames.gimbal {
	 surface = toon.flat {
	    color = options.carom and {0.1, 0.1, 0} or {0, 0.1, 0},

	    toon.shadow {
	       position = {0, 0, -billiards.ballradius + 1e-3},
	       mesh = meshes.ball(){
		  position = {0, 0, billiards.ballradius}
	       }
	    }
	 }
      },
   }

   if options.annotate then
      local annotation = widgets.annotation {
	 color = {1.0, 0.8, 0.2}, 
	 opacity = 0.8,
	 thickness = 1.5,
	 padding = {0.01, 0.01},
	 radius = 0.07,
	 angle = 40,

	 table = widgets.layout {
	    prepare = function (self)
	       local ball = self
			 
	       while (not ball.isball) do
		  ball = ball.parent
	       end

	       self.text = string.format("<span font=\"Sans 10\" color=\"Khaki\">Speed: </span><span font=\"Sans 10\" color=\"White\">%.3f @ %d\194\176</span>\n<span font=\"Sans 10\" color=\"Khaki\">Spin: </span><span font=\"Sans 10\" color=\"White\">%.3f @ (%d\194\176, %d\194\176)</span>\n",
					 math.length(ball.velocity),
					 math.deg(math.atan2(ball.velocity[2],
							     ball.velocity[1])),
					 math.length(ball.spin),
					 math.deg(math.atan2(ball.spin[2],
							     ball.spin[1])),
					 math.deg(math.acos(math.normalize(ball.spin)[3])))
	    end
	 },
      }

      if options.annotate == true then
	 bodies.balls[i].surface.selected.annotation = annotation
      elseif type(options.annotate) == "number" then
	 if options.annotate == i then
	    bodies.balls[i].surface.annotation = annotation
	 end
      elseif type(options.annotate) == "table" then
	 for _, j in pairs (options.annotate) do
	    if i == j then
	       bodies.balls[i].surface.annotation = annotation
	    end
	 end
      end
   end
end

bodies.cueball = bodies.balls[1]

-- Add them to the scene.

graph.gear = bodies.system {

   ambience = not options.toon and shading.ambient {
      intensity = resources.clamped(options.pool and
				    "billiards/imagery/light/poolambience.lc" or
				    "billiards/imagery/light/caromambience.lc")
   },

   finish = function (self)
   		local isstable = true
		
   		-- Check whether the shot is finished. A ball is
   		-- stable if it is either resting or off the table
		
   		for _, ball in pairs (bodies.balls) do
   		   if not (ball.isout or
   			   ball.isresting or
   			   ball.ispocketed) then
   		      isstable = false
   		   end
   		end

   		-- Finished?

   		if not isstable and
   		   not bodies.observer.iswaiting then
   		   bodies.observer.iswaiting = true
   		end

   		if bodies.observer.iswaiting and isstable then
		   for _, ball in pairs(bodies.balls) do
		      ball.velocity = {0, 0, 0}
		      ball.spin = {0, 0, 0}
		   end

   		   callhooks (billiards.finished)

   		   bodies.observer.islooking = true
   		end
   	     end,
   
   unpack (bodies.balls)
}

-- Replace any balls that have fallen
-- off the table to their opening spots

billiards.looking.respot = function ()
			      for _, ball in pairs (bodies.balls) do
				 if ball.isout then
				    respotball (ball)
				 end
			      end
			   end
