-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "network"
require "pages"
require "resources"
require "bookmarks"
require "derived"

math.randomseed (os.time())

graphics.close = function ()
		    if options.interactive then
		       network.block = true
		       graphics.hide = true
		    else
		       common.iterate = false
		    end
		 end

-- Right now full shading only works on 'certain' GPUs.
-- The rest will have to do with fixed-pipeline toon
-- shading.

if not common.engineering and not options.notoon and
   (not string.find (configuration.graphics[1], "NVIDIA") or
    not string.find (configuration.graphics[4], "fragment_shader")) then
   print ("I'm going to take a wild guess that your card doesn't " .. 
	  "support full\n shading so I'm falling back to toon mode. " ..
	  "Override with -Onotoon.\n")
   options.toon = true
   options.poweroftwo = true
end

-- These values are fixed.

graphics.title = "Billiards"
dynamics.collision = {}
derived.frustum = {45, 0.001, 20}

billiards = {}
bindings = {}

-- Open the replay bookmarks.

network.archive = network.bookmark {
   path = os.getenv ("HOME") .. "/.billiards.d"
}

network.archive.tags = resources.loadcache (os.getenv ("HOME") .. "/.billiards.d/tags.lua")

-- Load the default configuration.

resources.dofile "billiards/common.lua"
resources.dofile "billiards/defaults.lua"
resources.dofile "billiards/resources.lua"

if not options.interactive and not (options.eightball or
				    options.nineball or
				    options.carom) then
   options.eightball = true
end

if options.eightball or
   options.nineball then
   options.pool = true
elseif options.carom then
   options.billiards = true
end

if options.experiment and not options.pool then
   options.billiards = true
end
 
-- Load per-user configuration.

print "Reading local user configuration."

local rcscript = loadfile (os.getenv ("HOME") .. "/.billiards")

if rcscript then
   print ("  " .. os.getenv ("HOME") .. "/.billiards")
   rcscript ()
end

resources.dofile "billiards/observer.lua"
resources.dofile "billiards/display.lua"

if type(options.run) ~= "table" then
   options.run = {options.run}
end

for _, line in pairs(options.run) do
   assert(loadstring(line))()
end

if options.fullscreen then
   graphics.window = configuration.screen
end

if not options.nohttpd then
   resources.dofile "billiards/httpd.lua"
end

if options.eightball or options.nineball or
   options.carom or options.experiment then
   resources.dofile "billiards/restart.lua"
   graphics.hide = false
else
   network.block = true
   print "Waiting for http traffic."
end
