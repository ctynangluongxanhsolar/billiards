-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "transforms"
require "transitions"

-- Set up the controls.

graph.controls = frames.event {
   keypress = function (self, key)
      if key == 'escape' then
	 if options.interactive then
	    network.block = true
	    graphics.hide = true
	 else
	    common.iterate = false
	 end
      elseif key == 'p' then
	 if dynamics.oldtimescale then
	    dynamics.timescale = dynamics.oldtimescale
	    dynamics.oldtimescale = nil
	 else
	    dynamics.oldtimescale = dynamics.timescale
	    dynamics.timescale = 0
	 end
      elseif key == 'space' then
	 if bodies.observer.iswaiting then
	    dynamics.oldtimescale = dynamics.timescale
	    dynamics.timescale = 10

	    billiards.looking.resetscale = function ()
	       dynamics.timescale = dynamics.oldtimescale
	       billiards.looking.resetscale = nil
	    end
	 end
      elseif key == 'r' then
      else
	 print ("('" .. key .. "' pressed)")
      end
   end,

   keyrelease = function (self, key)
      print ("('" .. key .. "' released)")
   end
}

-- Now for the simulation and control logic.

billiards.looking.reset = function ()
   -- When a new shot begins:
   -- Do a crossfade.

   graph.transition = transitions.dissolve {
      duration = 0.7
   }

   -- Set up the camera.

   bodies.observer.radius = 1.5
   
   if bodies.cueball.ispocketed then
      bodies.observer.azimuth = math.rad(0)
      bodies.observer.elevation = math.rad(80)
      bodies.observer.longitude = 0.5 * billiards.tablewidth + 0.4
      bodies.observer.latitude = 0
   else
      bodies.observer.longitude = bodies.cueball.position[1]
      bodies.observer.latitude = bodies.cueball.position[2]
   end
end

billiards.aiming.reset = function ()
   -- Dissolve.
			    
   graph.transition = transitions.dissolve {
      duration = 0.7
   }

   -- Reset the observer and cue to default
   -- orientations when aiming.

   bodies.cue.elevation = 0
   bodies.cue.sidespin = 0
   bodies.cue.follow = 0
   
   bodies.observer.longitude = bodies.cueball.position[1]
   bodies.observer.latitude = bodies.cueball.position[2]
   bodies.observer.radius = 0.4
   bodies.observer.elevation = math.pi / 2 -
      bodies.cue.elevation -
      math.rad (7)
end

billiards.turning.aim = function ()
   -- Coordinate your head and hands when
   -- aiming.

   bodies.cue.azimuth = bodies.observer.azimuth
end

billiards.waiting.standup = function ()
   -- Zoom out once you've taken a shot.

   bodies.observer.radius = 1.5
end

-- Shot diagrams.

billiards.looking.starttracking = function ()
      graph.gear.tracker = nil

      -- Initialize ball trajectory logs.

      for _, ball in ipairs (bodies.balls) do
	 ball.trajectory = {}
	 ball.arclength = 0
	 ball.oldvelocity = {0, 0, 0}
      end
   end

billiards.waiting.trackballs =
   function ()
      for _, ball in ipairs (bodies.balls) do
	 local record

	 record = ball.position
	 record[4] = 0

	 table.insert(ball.trajectory, record)
	 table.insert(ball.trajectory, record)
      end

      -- Start logging ball trajectories.

      graph.gear.tracker = frames.timer {
	 period = 0.01,

	 tick = function (self, tick, delta, elapsed)
	    local record

	    for _, ball in ipairs (bodies.balls) do
	       if not ball.ispocketed then
		  local v_0 = ball.oldvelocity
		  local v = ball.velocity
		  local S_0 = ball.arclength
		  local S = math.distance (ball.position,
					   ball.trajectory[#ball.trajectory])
		  local C = math.distance (ball.position,
					   ball.trajectory[#ball.trajectory - 1]) 
		  
		  if 0.5 * math.sqrt((S_0 + S) * (S_0 + S) -
			             C * C) > billiards.tracking[1] or
		     math.distance (v_0, v) > billiards.tracking[2] then
		     ball.arclength = S
		     ball.oldvelocity = v
		  else
		     table.remove (ball.trajectory)
		     ball.arclength = S_0 + S
		  end

		  record = ball.position
		  record[4] = elapsed

		  table.insert (ball.trajectory, record)
	       end
	    end
	 end
      }
   end

-- Shot history.

billiards.striking.saveshot = function ()
    local shot = {}

    -- Save the shot trajectories.

    for _, ball in ipairs (bodies.balls) do
       table.insert(shot, ball.trajectory)
    end

    table.insert (billiards.history, shot)
end

billiards.cuecollision.initial = function ()
    bodies.cueball.poststep = function (self)
        local shot = billiards.history[#billiards.history]

        shot.initial = {self.index, self.position, self.velocity, self.spin}
	self.poststep = nil
    end
end
