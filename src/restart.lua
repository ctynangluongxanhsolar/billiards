-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

local hidden = graphics.hide

graphics.hide = true
graphics.canvas = options.toon and {0.7, 0.7, 0.6} or {0.07, 0.07, 0.075}
dynamics.drawvolumes = options.drawvolumes and true or false

billiards.started = true

billiards.waiting = {}
billiards.looking = {}
billiards.aiming = {}
billiards.finished = {}
billiards.ballcollision = {}
billiards.cushioncollision = {}
billiards.cuecollision = {}
billiards.striking = {}
billiards.turning = {}
billiards.zooming = {}
billiards.panning = {}
billiards.reorienting = {}
billiards.adjusting = {}
billiards.grabbing = {}
billiards.releasing = {}
billiards.setuprun = {}
billiards.recordrun = {}

if not billiards.history then
   billiards.history = {}
end

if options.experiment and type (options.experiment) == "string" then
   resources.dofile (options.experiment)
end

resources.dofile "billiards/collision.lua"
resources.dofile "billiards/room.lua"
resources.dofile "billiards/cue.lua"
resources.dofile "billiards/billiard.lua"
resources.dofile "billiards/rack.lua"

if options.experiment then
   resources.dofile "billiards/experiment.lua"
else 
   resources.dofile "billiards/game.lua"
end

bodies.observer.islooking = true
bodies.observer.iswaiting = true

-- Tie a few more loose strings.

graph.framerate = options.framerate and widgets.display {
   align = {1, 1},

   gauge = widgets.speedometer {
      color = {1.0, 0.8, 0.2},
      opacity = 0.6,
   }
}

if options.slow then
   billiards.cuecollision.slow = function ()
				    -- Start slow-motion once the
				    -- ball is struck.

				    dynamics.timescale = tonumber(options.slow) or 0.25
				 end

   billiards.looking.slow = function ()
			       -- End it when the shot is done.
			       
			       dynamics.timescale = 1
			    end
end

if not hidden then
   graphics.hide = false
end