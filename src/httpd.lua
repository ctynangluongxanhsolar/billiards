-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "network"
require "moremath"

print "\nLoading the http server pages."

network.mime["/stylesheet"] = "text/css"
network.pages["/stylesheet"] = resources.readfile "billiards/pages/stylesheet.lua"

network.mime["/test"] = "text/html"
network.pages["/test"] = resources.readfile "billiards/pages/test.lua"

network.mime["/settings"] = "text/html"
network.pages["/settings"] = resources.readfile "billiards/pages/settings.lua"

network.mime["/history"] = "text/html"
network.pages["/history"] = resources.readfile "billiards/pages/history.lua"

network.mime["/logo"] = "image/svg+xml"
network.pages["/logo"] = resources.readfile "billiards/pages/logo.lua"

network.pages["/"] = resources.readfile "billiards/pages/default.lua"
network.mime["/"] = "text/html"

network.pages["/newgame"] = resources.readfile "billiards/pages/newgame.lua" 
network.mime["/newgame"] = "text/html"

network.pages["/setvalue"] = resources.readfile "billiards/pages/setvalue.lua" 
network.mime["/setvalue"] = "text/plain"

network.pages["/getvalue"] = resources.readfile "billiards/pages/getvalue.lua" 
network.mime["/getvalue"] = "text/plain"

network.mime["/archives"] = "text/html"
network.pages["/archives"] = resources.readfile "billiards/pages/archives.lua"

network.mime["/replay"] = "text/plain"
network.pages["/replay"] = resources.readfile "billiards/pages/replay.lua" 

network.mime["/backtrack"] = "text/plain"
network.pages["/backtrack"] = resources.readfile "billiards/pages/backtrack.lua" 
do
   local archive = resources.readfile "billiards/pages/archive.lua"

   network.mime["/archive"] = "text/plain"
   network.pages["/archive"] = function (method, uri, version)
				  if not bodies.observer.iswaiting then
				     return archive
				  end
			       end
end

do
   local export = resources.readfile "billiards/pages/export.lua"

   network.mime["/export"] = "application/x-lua"
   network.pages["/export"] = function (method, uri, version)
				 if not bodies.observer.iswaiting then
				    return method ~= "HEAD" and export or ""
				 end
			      end
end

do
   local drawpool = resources.readfile "billiards/pages/drawpool.lua"
   local drawbilliards = resources.readfile "billiards/pages/drawbilliards.lua"

   network.pages["/drawtable"] = function ()
				    if options.pool then
				       return drawpool
				    elseif options.billiards then
				       return drawbilliards
				    end
				 end
end

network.mime["/drawtable"] = "image/svg+xml"

do
   local shot = 0
   local inprogress = false

   network.mime["/updateshot"] = "text/plain"
   network.pages["/updateshot"] = function (method, url, version)
				  -- Only return a page when a new
				  -- shot has been made.

				  if #billiards.history ~= shot then
				     shot = #billiards.history
				     inprogress = true
				     
				     return tostring(shot)
				  end

				  if inprogress and
				     bodies.observer.islooking then
				     inprogress = false
				     
				     return tostring(shot)
				  end
			       end
end	 
