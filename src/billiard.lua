-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "dynamics"
require "physics"
require "moremath"

local F = (billiards.rollingfriction * 
	   billiards.ballmass *
	   math.abs(dynamics.gravity[3]))

local tau_p = (0.4 * billiards.rollingfriction * billiards.ballmass *
	       billiards.ballradius * math.abs(dynamics.gravity[3]))
local tau_z = (billiards.spinningfriction * billiards.ballmass *
	       billiards.ballradius * math.abs(dynamics.gravity[3]))

bodies.simpleball = bodies.simpleball or bodies.ball

function bodies.ball (values)
   local meta, self

   self = bodies.simpleball {
      radius = billiards.ballradius,
      mass = physics.spheremass (3 * billiards.ballmass /
				 (4 * math.pi *
				  billiards.ballradius^3),
			         billiards.ballradius),

      lethargy = {billiards.thresholds[1], billiards.thresholds[2], 2, 0},

      isball = true,

      logger = nil and frames.timer {
	 period = 0,
	 
	 link = function (self)
		   speed = io.open ("SPEED", "w")
		end,

	 unlink = function (self)
		     io.close (speed)
		  end,

	 tick = function (self, n, delta, elapsed)
	    if not self.parent.isresting then
	       local u = self.parent.velocity
	       local v = math.cross (self.parent.spin, {0, 0, self.parent.radius})
 	       speed:write (string.format ("%.5f %.5e\n",
					   elapsed,
					   math.length (u)))
					   -- math.length (math.subtract (u, v))))
	    end
	 end
      },
  
      -- The rolling friction torque is derived thus:
      -- |w| = |v| / r <=> |alpha| = |a| / r <=> |tau| / I = |F| / mr
      -- |tau| = I|F| / mr = 2|F|r / 5 = 2 mu mgr / 5
    
      step = function (self)
         if self.position[3] < billiards.ballradius + 1e-3 then
      	    local v, z = self.velocity, self.spin[3]
      	    local w = {self.spin[1], self.spin[2], 0}

      	    local V, Z = math.length (v), math.abs(z)
      	    local W = math.length (w)

      	    if V > billiards.thresholds[1] then
      	       physics.addforce (self, math.scale (v, -F / V))
      	    end

      	    if W > billiards.thresholds[2] then
      	       physics.addtorque (self, math.scale (w, -tau_p / W))
	    end
	    
      	    if Z > billiards.thresholds[2] then
      	       physics.addtorque (self, {0, 0, -tau_z * z / Z})
      	    end
      	 end

         if self.home then
	    physics.addforce (self,
			      {10 * (self.home[1] - self.position[1]) -
			       3.5 * self.velocity[1],
			       10 * (self.home[2] - self.position[2]) -
			       3.5 * self.velocity[2],
			       200 * (self.home[3] - self.position[3]) -
			       15 * self.velocity[3]})
	 end
      end,
   }

   meta = getmetatable (self)
   meta.oldindex = meta.__index
   meta.oldnewindex = meta.__newindex

   meta.__index = function (self, key)
      local meta = getmetatable(self)

      if key == "isout" then
	 return -- math.abs(self.position[1]) > 0.5 * billiards.tablewidth + 0.3 or
-- 	        math.abs(self.position[2]) > 0.5 * billiards.tableheight + 0.3 or
	        self.position[3] < -0.5
      elseif key == "ispocketed" then
	 return math.abs(self.position[1]) < 0.5 * billiards.tablewidth + 0.15 and
	        math.abs(self.position[2]) < 0.5 * billiards.tableheight + 0.15 and
	        self.position[3] < -billiards.ballradius
      elseif key == "isresting" then
	 return math.length {self.velocity[1], self.velocity[2], 0} <
	        billiards.thresholds[1] and
	        math.length2 (self.spin) < billiards.thresholds[2]
      elseif key == "isrolling" then
	 local u = self.velocity
	 local v = math.cross (self.spin, {0, 0, self.radius})

	 return math.length (math.subtract (u, v)) < billiards.thresholds[1] and
	        not self.isresting
      else
	 return meta.oldindex (self, key)
      end
   end

   meta.__newindex = function (self, key, value)
      local meta = getmetatable(self)

      if key == "isout" then
      elseif key == "ispocketed" then
      elseif key == "isresting" then
      elseif key == "isrolling" then
      else
	 meta.oldnewindex(self, key, value)
      end
   end

   meta.__tostring = function(self)
			 return "Billiard"
		      end

   for key, value in pairs(values) do
      self[key] = value
   end

   return self
end
