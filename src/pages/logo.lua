<?xml version="1.0" encoding="UTF-8" standalone="no"?>

<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!-- Created with Inkscape (http://www.inkscape.org/) -->
<svg
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   version="1.0"
   width="229"
   height="85"
   viewBox="0 0 336.29999 124.92191"
   id="svg2">
  <defs
     id="defs4">
    <marker
       refX="0"
       refY="0"
       orient="auto"
       id="RazorWire">
       style=&quot;overflow:visible&quot;&gt;
      <path
   d="M 0.022727273,-0.74009011 L 0.022727273,0.69740989 L -7.7585227,3.0099099 L 10.678977,3.0099099 L 3.4914773,0.69740989 L 3.4914773,-0.74009011 L 10.741477,-2.8963401 L -7.7272727,-2.8963401 L 0.022727273,-0.74009011 z "
   transform="scale(0.8,0.8)"
   style="fill:#808080;fill-opacity:1;fill-rule:evenodd;stroke:#000000;stroke-width:0.1pt"
   id="path7910" />
</marker>
    <marker
       refX="0"
       refY="0"
       orient="auto"
       style="overflow:visible"
       id="Torso">
      <g
         transform="scale(0.7,0.7)"
         id="g7890">
        <path
           d="M -4.7792281,-3.239542 C -2.4288541,-2.8736027 0.52103922,-1.3019943 0.25792722,0.38794346 C -0.0051877922,2.0778819 -2.2126741,2.6176539 -4.5630471,2.2517169 C -6.9134221,1.8857769 -8.521035,0.75201414 -8.257922,-0.93792336 C -7.994809,-2.6278615 -7.1296041,-3.6054813 -4.7792281,-3.239542 z "
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:#000000;stroke-width:1.25;marker-start:none;marker-mid:none;marker-end:none"
           id="path7892" />
        <path
           d="M 4.4598789,0.088665736 C -2.5564571,-4.378332 5.2248769,-3.9061806 -0.84829578,-8.7197331"
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:#000000;stroke-width:1pt;marker-end:none"
           id="path7894" />
        <path
           d="M 4.9298719,0.057520736 C -1.3872731,1.7494689 1.8027579,5.4782079 -4.9448731,7.5462725"
           style="fill:none;fill-opacity:0.75;fill-rule:evenodd;stroke:#000000;stroke-width:1pt;marker-start:none;marker-end:none"
           id="path7896" />
        <rect
           width="2.6366582"
           height="2.7608147"
           x="-10.391706"
           y="-1.7408575"
           transform="matrix(0.527536,-0.849533,0.887668,0.460484,0,0)"
           style="fill-rule:evenodd;stroke-width:1pt;marker-end:none"
           id="rect7898" />
        <rect
           width="2.7327356"
           height="2.8614161"
           x="4.9587269"
           y="-7.9629307"
           transform="matrix(0.671205,-0.741272,0.790802,0.612072,0,0)"
           style="fill-rule:evenodd;stroke-width:1pt;marker-end:none"
           id="rect7900" />
        <path
           d="M 16.779951 -28.685045 A 0.60731727 0.60731727 0 1 1  15.565317,-28.685045 A 0.60731727 0.60731727 0 1 1  16.779951 -28.685045 z"
           transform="matrix(0,-1.109517,1.109517,0,25.96648,19.71619)"
           style="fill:#ff0000;fill-opacity:0.75;fill-rule:evenodd;stroke:#000000;stroke-width:1pt;marker-start:none;marker-end:none"
           id="path7902" />
        <path
           d="M 16.779951 -28.685045 A 0.60731727 0.60731727 0 1 1  15.565317,-28.685045 A 0.60731727 0.60731727 0 1 1  16.779951 -28.685045 z"
           transform="matrix(0,-1.109517,1.109517,0,26.8245,16.99126)"
           style="fill:#ff0000;fill-opacity:0.75;fill-rule:evenodd;stroke:#000000;stroke-width:1pt;marker-start:none;marker-end:none"
           id="path7904" />
      </g>
    </marker>
    <marker
       refX="0"
       refY="0"
       orient="auto"
       style="overflow:visible"
       id="SquareS">
      <path
         d="M -5,-5 L -5,5 L 5,5 L 5,-5 L -5,-5 z "
         transform="scale(0.2,0.2)"
         style="fill-rule:evenodd;stroke:#000000;stroke-width:1pt;marker-start:none"
         id="path7817" />
    </marker>
    <marker
       refX="0"
       refY="0"
       orient="auto"
       style="overflow:visible"
       id="Arrow2Lstart">
      <path
         d="M 8.7185878,4.0337352 L -2.2072895,0.016013256 L 8.7185884,-4.0017078 C 6.97309,-1.6296469 6.9831476,1.6157441 8.7185878,4.0337352 z "
         transform="matrix(1.1,0,0,1.1,1.1,0)"
         style="font-size:12px;fill-rule:evenodd;stroke-width:0.625;stroke-linejoin:round"
         id="path7934" />
    </marker>
    <linearGradient
       id="linearGradient3134">
      <stop
         style="stop-color:#000000;stop-opacity:1"
         offset="0"
         id="stop3136" />
      <stop
         style="stop-color:#000000;stop-opacity:0"
         offset="1"
         id="stop3138" />
    </linearGradient>
  </defs>
  <g
     transform="translate(-4.1023177,-439.5842)"
     style="display:inline"
     id="layer1">
    <rect
       width="161.8746"
       height="48.177563"
       rx="0"
       ry="19.391598"
       x="-0.46451232"
       y="492.46902"
       style="fill:#efc87c;fill-opacity:1;fill-rule:evenodd;stroke:#2b1100;stroke-width:2.5017941;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect2174" />
    <rect
       width="144.70752"
       height="11.089672"
       rx="0.46626887"
       ry="11.089672"
       x="9.7036438"
       y="526.34717"
       style="fill:#8c5e3d;fill-opacity:1;fill-rule:evenodd;stroke:#ffffff;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect8731" />
    <rect
       width="36.106884"
       height="48.162033"
       rx="0"
       ry="19.385344"
       x="163.6806"
       y="492.47684"
       style="opacity:0.98999999;fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#2b1100;stroke-width:2.43036246;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect3147" />
    <rect
       width="28.53397"
       height="11.236521"
       rx="0.69939458"
       ry="11.236521"
       x="167.17433"
       y="526.27374"
       style="fill:#8c8c8c;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect8733" />
    <rect
       width="145.01897"
       height="4.5101757"
       rx="0.41586015"
       ry="4.5101757"
       x="9.5479212"
       y="495.69589"
       style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0.43278965;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect10681" />
    <rect
       width="28.6194"
       height="5.040103"
       rx="0.28580904"
       ry="5.040103"
       x="167.13162"
       y="495.43094"
       style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0.43278965;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect10683" />
    <path
       d="M 201.22291,492.01149 L 201.22291,541.10417 C 201.26098,541.10463 201.29864,541.10417 201.3368,541.10417 C 208.60865,541.10417 214.46193,532.43752 214.46193,521.66931 L 214.46193,511.46041 C 214.46193,500.69215 208.60865,492.01149 201.3368,492.01149 C 201.29864,492.01149 201.26098,492.01098 201.22291,492.01149 z "
       style="opacity:0.98999999;fill:#253e7f;fill-opacity:1;fill-rule:evenodd;stroke:#000c3d;stroke-width:1.39621198;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect3149" />
    <path
       d="M 213.02446,520.56458 C 210.73011,524.24797 207.22447,526.42282 203.20614,526.3015 L 203.18856,537.44721 C 208.59587,537.95068 212.89934,530.60651 213.02446,520.56458 z "
       style="opacity:0.98999999;fill:#000c3d;fill-opacity:1;fill-rule:evenodd;stroke:#230800;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="rect3156" />
    <path
       d="M 631.42857 583.79077 A 134.28572 134.28572 0 1 1  362.85713,583.79077 A 134.28572 134.28572 0 1 1  631.42857 583.79077 z"
       transform="matrix(0.4566336,0,0,0.4566336,50.113285,235.46667)"
       style="opacity:0.98999999;fill:#ffe776;fill-opacity:1;fill-rule:evenodd;stroke:#796433;stroke-width:5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="path2160" />
    <path
       d="M 319.91113,468.17158 C 331.56542,485.7037 331.39269,509.81595 317.64297,528.77576 C 299.84325,553.32015 265.99345,559.9816 242.08572,543.62876 C 238.8075,541.38648 235.88982,538.82126 233.33402,536.01148 C 236.76904,541.18588 241.23077,545.79121 246.67942,549.51805 C 270.58714,565.87088 304.43694,559.20944 322.23667,534.66504 C 337.60206,513.47741 336.01321,485.85245 319.91113,468.17158 z "
       style="fill:#f2a036;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="path5811" />
    <path
       d="M 512.85715 563.07648 A 32.142857 39.285713 0 1 1  448.57144,563.07648 A 32.142857 39.285713 0 1 1  512.85715 563.07648 z"
       transform="matrix(0.1049307,0.1499108,-0.2079091,0.162839,312.28963,305.49807)"
       style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#ebe26b;stroke-width:2.92016935;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       id="path6785" />
    <text
       x="424.04767"
       y="404.65524"
       style="font-size:12px;font-style:normal;font-weight:normal;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;font-family:Bitstream Vera Sans"
       id="text10677"
       xml:space="preserve"><tspan
         x="424.04767"
         y="404.65524"
         id="tspan10679">   </tspan></text>
    <g
       transform="matrix(1.4426321,0,0,1.4426321,-10.431158,-194.57406)"
       id="g10059">
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="translate(10.913092,441.465)"
         style="opacity:0.98999999;fill:#0c7aff;fill-opacity:0.56092434;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path6758" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(1.7142858,0,0,1.7142858,-91.017103,411.19986)"
         style="opacity:0.98999999;fill:#0c7aff;fill-opacity:0.75210081;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path7729" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5,0,0,0.5,84.119493,458.74335)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.90756303;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path7731" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.8928571,0,0,0.8928571,27.835287,437.57213)"
         style="opacity:0.98999999;fill:#2d6cb8;fill-opacity:0.68277313;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path7956" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.7857143,0,0,0.7857143,44.111067,446.12274)"
         style="opacity:0.98999999;fill:#4961c7;fill-opacity:0.90756303;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8927" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(1.4107144,0,0,1.4107144,-54.063084,420.96952)"
         style="opacity:0.98999999;fill:#8fafd6;fill-opacity:0.68277313;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8929" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.9285714,0,0,0.9285714,18.935549,439.97402)"
         style="opacity:0.98999999;fill:#2d6cb8;fill-opacity:0.68277313;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8931" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5714285,0,0,0.5714285,64.219171,452.88135)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8933" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.8571428,0,0,0.8571428,26.877209,432.90778)"
         style="opacity:0.98999999;fill:#2d6cb8;fill-opacity:0.68277313;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8935" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.3214285,0,0,0.3214285,109.87218,467.41905)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8937" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.7500001,0,0,0.7500001,34.264797,441.86239)"
         style="opacity:0.98999999;fill:#0c7aff;fill-opacity:0.75210081;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8939" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.8928573,0,0,0.8928573,16.684627,431.5928)"
         style="opacity:0.98999999;fill:#0cb7ff;fill-opacity:0.50630254;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8941" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,89.048398,457.0836)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8943" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5714285,0,0,0.5714285,75.450624,438.98344)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8945" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,94.260111,444.84214)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8947" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,104.94325,488.21737)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8949" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.3214285,0,0,0.3214285,111.6181,488.17622)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8951" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5714285,0,0,0.5714285,78.58335,485.53054)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8953" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,102.28644,493.97379)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8955" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.2936365,0,0,0.2936365,118.73939,497.32984)"
         style="opacity:0.98999999;fill:#567e94;fill-opacity:0.91386557;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8957" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,103.34346,496.58772)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8959" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.9285714,0,0,0.9285714,26.38984,474.37021)"
         style="opacity:0.98999999;fill:#2d6cb8;fill-opacity:0.68277313;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8961" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.607143,0,0,0.607143,72.715458,488.39068)"
         style="opacity:0.98999999;fill:#0cb7ff;fill-opacity:0.50630254;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8963" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.2142857,0,0,0.2142857,110.79562,444.69936)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.90756303;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8965" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,76.645326,443.4685)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8967" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.3214285,0,0,0.3214285,89.510131,447.70341)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8969" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5714285,0,0,0.5714285,50.240472,448.59885)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8971" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,98.623407,499.14092)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8973" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.5714285,0,0,0.5714285,80.056325,492.4742)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8975" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.2142857,0,0,0.2142857,125.74394,508.53277)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.90756303;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8977" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.3214285,0,0,0.3214285,110.8418,500.87098)"
         style="opacity:0.98999999;fill:#2d86b8;fill-opacity:0.59453777;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8979" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.2142857,0,0,0.2142857,122.18867,506.59352)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.90756303;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8981" />
      <path
         d="M 142.85753 32.294262 A 2.2624493 2.2624493 0 1 1  138.33263,32.294262 A 2.2624493 2.2624493 0 1 1  142.85753 32.294262 z"
         transform="matrix(0.375,0,0,0.375,98.78501,506.25148)"
         style="opacity:0.98999999;fill:#0c3bff;fill-opacity:0.71848737;fill-rule:evenodd;stroke:#ebe26b;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         id="path8983" />
    </g>
    <g
       transform="matrix(1.4426321,0,0,1.4426321,-10.431158,-194.57406)"
       id="g9282">
      <path
         d="M 34.6875,8.78125 L 34.6875,9.75 L 35,9.78125 L 36.03125,9.84375 C 36.635414,9.88544 37.031248,9.9896004 37.1875,10.15625 C 37.354163,10.31252 37.458331,10.68752 37.5,11.28125 L 37.59375,12.84375 L 37.59375,29.40625 L 37.5,30.96875 C 37.45833,31.5625 37.354164,31.927081 37.1875,32.09375 C 37.031247,32.25 36.635415,32.36458 36.03125,32.40625 L 35,32.46875 L 34.6875,32.5 L 34.6875,33.46875 L 43.5625,33.46875 L 43.5625,32.5 L 43.28125,32.46875 L 42.21875,32.40625 C 41.562492,32.36458 41.124994,32.16667 40.9375,31.8125 C 40.749993,31.44792 40.656244,30.635409 40.65625,29.40625 L 40.65625,8.78125 L 34.6875,8.78125 z M 44.75,8.78125 L 44.75,9.75 L 45.0625,9.78125 L 46.09375,9.84375 C 46.697914,9.88544 47.093748,9.9896004 47.25,10.15625 C 47.416663,10.31252 47.520831,10.68752 47.5625,11.28125 L 47.65625,12.84375 L 47.65625,29.40625 L 47.5625,30.96875 C 47.52083,31.5625 47.416664,31.927081 47.25,32.09375 C 47.093747,32.25 46.697915,32.36458 46.09375,32.40625 L 45.0625,32.46875 L 44.75,32.5 L 44.75,33.46875 L 53.625,33.46875 L 53.625,32.5 L 53.34375,32.46875 L 52.28125,32.40625 C 51.624992,32.36458 51.187494,32.16667 51,31.8125 C 50.812493,31.44792 50.718744,30.635409 50.71875,29.40625 L 50.71875,8.78125 L 44.75,8.78125 z M 106.5625,8.78125 L 106.5625,9.75 L 106.875,9.78125 L 107.9375,9.84375 C 108.59374,9.88544 109.01041,10.10419 109.1875,10.46875 C 109.37499,10.82294 109.46874,11.61461 109.46875,12.84375 L 109.46875,18.4375 C 108.05208,16.89585 106.35416,16.12502 104.375,16.125 C 102.21875,16.12502 100.48958,16.97918 99.1875,18.6875 C 97.88542,20.38543 97.218747,22.635419 97.21875,25.46875 C 97.21875,28.02083 97.80209,30.07292 98.9375,31.59375 C 100.07292,33.10417 101.58333,33.843741 103.5,33.84375 C 105.95832,33.84374 107.95833,32.66667 109.46875,30.28125 L 109.46875,33.46875 L 115.4375,33.46875 L 115.4375,32.5 L 115.15625,32.46875 L 114.09375,32.40625 C 113.42707,32.36458 112.99998,32.16667 112.8125,31.8125 C 112.6354,31.44792 112.56249,30.635409 112.5625,29.40625 L 112.5625,8.78125 L 106.5625,8.78125 z M 5.46875,10.34375 L 5.46875,11.28125 L 5.78125,11.3125 L 6.8125,11.375 C 7.416664,11.41668 7.8124968,11.52085 7.96875,11.6875 C 8.135413,11.85418 8.2395803,12.25002 8.28125,12.84375 L 8.375,14.375 L 8.375,29.40625 L 8.28125,30.96875 C 8.23958,31.5625 8.1354128,31.927081 7.96875,32.09375 C 7.812497,32.25 7.4166638,32.36458 6.8125,32.40625 L 5.78125,32.46875 L 5.46875,32.5 L 5.46875,33.46875 L 14.53125,33.46875 C 17.208321,33.46875 19.197901,32.98958 20.5,32.03125 C 21.802066,31.07292 22.468731,29.61459 22.46875,27.65625 C 22.468732,26.04167 21.906232,24.68751 20.8125,23.59375 C 19.718735,22.50001 18.208319,21.781259 16.28125,21.46875 C 19.760401,20.17709 21.499982,18.18752 21.5,15.46875 C 21.499983,13.8021 20.958316,12.53127 19.84375,11.65625 C 18.729152,10.78127 17.093737,10.34377 14.96875,10.34375 L 5.46875,10.34375 z M 11.59375,11.375 L 12.40625,11.375 C 14.520823,11.37502 15.989571,11.70835 16.84375,12.375 C 17.70832,13.03127 18.156235,14.1771 18.15625,15.8125 C 18.156236,19.21876 16.291655,20.93751 12.5625,20.9375 L 11.59375,20.9375 L 11.59375,11.375 z M 72.90625,16.125 C 71.072909,16.12502 69.072908,16.583349 66.90625,17.53125 L 66.90625,20.65625 L 69,20.65625 L 69,20.34375 C 69.197911,18.31251 70.322907,17.31252 72.34375,17.3125 C 74.374989,17.31252 75.374985,18.635429 75.375,21.3125 L 75.375,23.4375 L 74.75,23.4375 C 68.666662,23.43751 65.624995,25.31251 65.625,29.0625 C 65.624998,30.4375 66.104161,31.58334 67.0625,32.5 C 68.020829,33.40625 69.218742,33.843741 70.65625,33.84375 C 72.447908,33.84374 74.260403,32.937499 76.09375,31.125 C 76.499987,32.9375 77.458317,33.843741 78.96875,33.84375 C 79.562484,33.84374 80.229147,33.72917 80.96875,33.46875 L 80.875,32.3125 C 80.520817,32.40625 80.229147,32.437501 80.03125,32.4375 C 78.979151,32.4375 78.468732,31.208329 78.46875,28.75 L 78.46875,21.1875 C 78.468735,17.80209 76.614568,16.12502 72.90625,16.125 z M 93.5,16.125 C 91.22916,16.12502 89.374989,17.31252 87.96875,19.6875 L 87.96875,16.5 L 82,16.5 L 82,17.46875 L 82.3125,17.46875 L 83.34375,17.5625 C 83.947914,17.60418 84.343744,17.70835 84.5,17.875 C 84.666663,18.04168 84.770827,18.406259 84.8125,19 L 84.90625,20.53125 L 84.90625,29.40625 L 84.8125,30.96875 C 84.77083,31.5625 84.66666,31.927081 84.5,32.09375 C 84.343747,32.25 83.947911,32.36458 83.34375,32.40625 L 82.3125,32.46875 L 82,32.5 L 82,33.46875 L 91.25,33.46875 L 91.25,32.5 L 90.59375,32.46875 L 89.53125,32.40625 C 88.874992,32.36458 88.43749,32.16667 88.25,31.8125 C 88.062493,31.44792 87.96874,30.635409 87.96875,29.40625 L 87.96875,21.125 C 88.958326,19.47918 90.229157,18.65626 91.78125,18.65625 C 92.29166,18.65626 92.572907,18.916679 92.625,19.46875 L 92.78125,20.65625 L 92.8125,20.9375 L 94.53125,20.9375 L 94.53125,16.28125 C 94.14582,16.16668 93.81249,16.12502 93.5,16.125 z M 124.15625,16.125 C 122.45833,16.12502 121.07292,16.57293 119.96875,17.46875 C 118.875,18.3646 118.3125,19.50001 118.3125,20.875 C 118.3125,23.09376 119.9375,24.82293 123.15625,26.0625 L 124.71875,26.65625 C 126.54166,27.35417 127.46874,28.42709 127.46875,29.84375 C 127.46874,30.73959 127.16666,31.458331 126.5625,32.03125 C 125.96874,32.59375 125.19791,32.875001 124.25,32.875 C 123.09375,32.875 122.13542,32.64583 121.375,32.15625 C 120.61458,31.65625 120.19792,30.989579 120.125,30.1875 L 120,28.5625 L 119.96875,28.28125 L 118.21875,28.28125 L 118.21875,32.59375 C 120.04166,33.4375 121.98958,33.843741 124.0625,33.84375 C 125.95832,33.84374 127.52083,33.364581 128.71875,32.40625 C 129.91666,31.44791 130.5,30.218749 130.5,28.71875 C 130.49999,27.625 130.1875,26.75001 129.5625,26.0625 C 128.9479,25.36459 127.85415,24.677089 126.28125,24.03125 L 124.84375,23.4375 C 123.38541,22.84376 122.42708,22.34376 121.9375,21.90625 C 121.45833,21.45835 121.21875,20.854169 121.21875,20.125 C 121.21875,19.23959 121.49999,18.510429 122.09375,17.9375 C 122.69791,17.35418 123.46875,17.062509 124.375,17.0625 C 126.27083,17.06251 127.29166,17.843759 127.4375,19.40625 L 127.5625,20.84375 L 127.5625,21.125 L 129.3125,21.125 L 129.3125,17.0625 C 127.54166,16.4271 125.81249,16.12502 124.15625,16.125 z M 24.875,16.5 L 24.875,17.46875 L 25.1875,17.46875 L 26.21875,17.5625 C 26.822914,17.60418 27.218746,17.70835 27.375,17.875 C 27.541663,18.04168 27.645829,18.406259 27.6875,19 L 27.78125,20.53125 L 27.78125,29.40625 L 27.6875,30.96875 C 27.64583,31.5625 27.541662,31.927081 27.375,32.09375 C 27.218747,32.25 26.822913,32.36458 26.21875,32.40625 L 25.1875,32.46875 L 24.875,32.5 L 24.875,33.46875 L 33.75,33.46875 L 33.75,32.5 L 33.46875,32.46875 L 32.40625,32.40625 C 31.749992,32.36458 31.312492,32.16667 31.125,31.8125 C 30.937493,31.44792 30.843742,30.635409 30.84375,29.40625 L 30.84375,16.5 L 24.875,16.5 z M 54.8125,16.5 L 54.8125,17.46875 L 55.125,17.46875 L 56.15625,17.5625 C 56.760414,17.60418 57.156248,17.70835 57.3125,17.875 C 57.479163,18.04168 57.583331,18.406259 57.625,19 L 57.71875,20.53125 L 57.71875,29.40625 L 57.625,30.96875 C 57.58333,31.5625 57.479164,31.927081 57.3125,32.09375 C 57.156247,32.25 56.760415,32.36458 56.15625,32.40625 L 55.125,32.46875 L 54.8125,32.5 L 54.8125,33.46875 L 63.6875,33.46875 L 63.6875,32.5 L 63.40625,32.46875 L 62.34375,32.40625 C 61.687492,32.36458 61.249994,32.16667 61.0625,31.8125 C 60.874993,31.44792 60.781244,30.635409 60.78125,29.40625 L 60.78125,16.5 L 54.8125,16.5 z M 104.78125,17.84375 C 106.30208,17.84377 107.86458,18.427089 109.46875,19.625 L 109.46875,29.1875 C 108.94791,29.94792 108.22916,30.583329 107.3125,31.09375 C 106.39583,31.59375 105.51041,31.843749 104.65625,31.84375 C 101.88542,31.84375 100.5,29.437499 100.5,24.625 C 100.5,20.09376 101.92708,17.84377 104.78125,17.84375 z M 11.59375,22 C 16.572905,22.00001 19.062484,23.92709 19.0625,27.78125 C 19.062485,29.15625 18.635402,30.260419 17.75,31.125 C 16.864571,31.97916 15.708322,32.406251 14.28125,32.40625 C 13.249991,32.40625 12.552075,32.291661 12.1875,32.0625 C 11.833326,31.83333 11.656243,31.41667 11.65625,30.78125 L 11.59375,29.40625 L 11.59375,22 z M 74.65625,24.40625 L 75.375,24.40625 L 75.375,30.78125 C 74.15624,31.60416 73.093738,32.03125 72.1875,32.03125 C 71.239576,32.03125 70.447907,31.70834 69.8125,31.0625 C 69.187495,30.41667 68.874992,29.60417 68.875,28.625 C 68.874995,25.81251 70.802073,24.40626 74.65625,24.40625 z "
         transform="translate(10.109375,439.58419)"
         style="font-size:32px;font-style:normal;font-weight:normal;writing-mode:lr-tb;fill:#2b1100;font-family:LucidaBright"
         id="text10673" />
      <g
         id="g3229">
        <path
           d="M 631.42857 583.79077 A 134.28572 134.28572 0 1 1  362.85713,583.79077 A 134.28572 134.28572 0 1 1  631.42857 583.79077 z"
           transform="matrix(2.5805897e-2,0,0,2.5805897e-2,25.683171,436.12797)"
           style="opacity:0.98999999;fill:#800000;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path10120" />
        <path
           d="M 40.930353,449.2789 C 41.588976,450.2697 41.579214,451.63236 40.802171,452.70385 C 39.79625,454.09093 37.883284,454.46739 36.532177,453.54324 C 36.346914,453.41652 36.182026,453.27155 36.03759,453.11276 C 36.231714,453.40518 36.483862,453.66545 36.791783,453.87606 C 38.142888,454.80022 40.055854,454.42376 41.061777,453.03667 C 41.930126,451.83929 41.840336,450.27811 40.930353,449.2789 z "
           style="fill:#550000;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path10122" />
        <path
           d="M 512.85715 563.07648 A 32.142857 39.285713 0 1 1  448.57144,563.07648 A 32.142857 39.285713 0 1 1  512.85715 563.07648 z"
           transform="matrix(1.320887e-2,1.769838e-2,-2.4481997e-2,2.0407563e-2,44.478738,429.47585)"
           style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#eb8e6b;stroke-width:2.90441465;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path10124" />
      </g>
      <g
         id="g3221">
        <path
           d="M 631.42857 583.79077 A 134.28572 134.28572 0 1 1  362.85713,583.79077 A 134.28572 134.28572 0 1 1  631.42857 583.79077 z"
           transform="matrix(2.5805897e-2,0,0,2.5805897e-2,55.337416,436.12797)"
           style="opacity:0.98999999;fill:#e3e3b6;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:5;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path11098" />
        <path
           d="M 70.584598,449.2789 C 71.243221,450.2697 71.233459,451.63236 70.456416,452.70385 C 69.450495,454.09093 67.537529,454.46739 66.186422,453.54324 C 66.001159,453.41652 65.836271,453.27155 65.691835,453.11276 C 65.885959,453.40518 66.138107,453.66545 66.446028,453.87606 C 67.797133,454.80022 69.710099,454.42376 70.716022,453.03667 C 71.584371,451.83929 71.494581,450.27811 70.584598,449.2789 z "
           style="fill:#666666;fill-opacity:1;fill-rule:evenodd;stroke:#5c5c5c;stroke-width:0;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path11100" />
        <path
           d="M 512.85715 563.07648 A 32.142857 39.285713 0 1 1  448.57144,563.07648 A 32.142857 39.285713 0 1 1  512.85715 563.07648 z"
           transform="matrix(1.320887e-2,1.769838e-2,-2.4481997e-2,2.0407563e-2,74.132983,429.47585)"
           style="fill:#ffffff;fill-opacity:1;fill-rule:evenodd;stroke:#e0e0e0;stroke-width:2.90441465;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
           id="path11102" />
      </g>
    </g>
  </g>
</svg>
