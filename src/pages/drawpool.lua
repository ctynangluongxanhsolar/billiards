<?xml version="1.0" standalone="no"?>

<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" 
	    "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

  <svg <?lua
           return query.width and string.format([[
       width="%dpx"
       height="%dpx"
				  ]],
				  query.width,
				  query.width *
				     (billiards.tableheight + 0.4) /
				     (billiards.tablewidth + 0.4))
	?>
       viewBox="<?lua
		  return string.format("%f %f %f %f",
		                       -0.5 * billiards.tablewidth - 0.2,
		                       -0.5 * billiards.tableheight - 0.2,
		                       billiards.tablewidth + 0.4,
		                       billiards.tableheight + 0.4)
		?>"
       xmlns="http://www.w3.org/2000/svg" version="1.1" 
       xmlns:xlink="http://www.w3.org/1999/xlink">

    <defs>
      <pattern id="stripe" patternUnits="userSpaceOnUse"
               x="<?lua return -billiards.ballradius ?>"
	       y="<?lua return -billiards.ballradius ?>"
	       width="<?lua return 2 * billiards.ballradius ?>"
	       height="<?lua return 2 * billiards.ballradius ?>"
	       viewBox="0 0 1 1" >
            
        <rect x="0" y="0.3" width="1" height="0.4" fill="white" />
      </pattern> 

      <g id="side">
	<path d="M <?lua return -0.5 * billiards.tablewidth + 0.042 ?>
	           <?lua return -0.5 * billiards.tableheight - 0.04 ?>
		 L -0.06
	           <?lua return -0.5 * billiards.tableheight - 0.04 ?>
		 L -0.07
	           <?lua return -0.5 * billiards.tableheight ?>
		 L <?lua return -0.5 * billiards.tablewidth + 0.06 ?>
	           <?lua return -0.5 * billiards.tableheight ?>
		 z"
	      stroke="rgb(50, 50, 50)" stroke-width="0.003"
  	      fill="rgb(110, 139,  61)" />
      </g>

      <g id="top">
	<path d="M <?lua return -0.5 * billiards.tablewidth - 0.04 ?>
	           <?lua return -0.5 * billiards.tableheight + 0.042 ?>
		 L <?lua return -0.5 * billiards.tablewidth ?>
	           <?lua return -0.5 * billiards.tableheight + 0.062 ?>
		 L <?lua return -0.5 * billiards.tablewidth ?>
	           <?lua return 0.5 * billiards.tableheight - 0.062 ?>
		 L <?lua return -0.5 * billiards.tablewidth - 0.04 ?>
	           <?lua return 0.5 * billiards.tableheight - 0.042 ?>
		 z"
	      stroke="rgb(50, 50, 50)" stroke-width="0.003"
  	      fill="rgb(110, 139,  61)" />
      </g>
      
      <g id="pocket">
	<path fill="rgb(30, 30, 30)" stroke="black" stroke-width="0.003"
	      d="M -0.06 0 A 0.065 0.07 0 1 1 0.06 0"/>
	<path fill="black" d="M -0.06 0 A 0.06 0.08 0 0 1 0.06 0"/>
	<path fill="black" d="M -0.061 0 A 0.10 0.10 0 0 0 0.061 0"/>
      </g>

      <g id="diamond" transform="translate(0 -0.0141) rotate(45)">
	<rect width="0.02" height="0.02" fill="white" stroke="none"/>
      </g>

      <g id="ball">
	<circle r="<?lua return billiards.ballradius ?>" stroke-width="0.003"/>
      </g>
    </defs>

    <g transform="scale(-1, 1)" id="table">
      <!-- The frame. -->

      <rect x="<?lua return -0.5 * billiards.tablewidth - 0.19 ?>"
	    y="<?lua return -0.5 * billiards.tableheight - 0.19 ?>"
	    rx="0.09"
            width="<?lua return billiards.tablewidth + 0.38 ?>"
	    height="<?lua return billiards.tableheight + 0.38 ?>"
	    fill="rgb(40, 30, 17)" 
  	    stroke="rgb(110, 90, 60)" stroke-width="0.015"/>

      <!-- The cloth. -->

      <rect x="<?lua return -0.5 * billiards.tablewidth - 0.04 ?>"
	    y="<?lua return -0.5 * billiards.tableheight - 0.04 ?>"
	    width="<?lua return billiards.tablewidth + 0.08 ?>"
            height="<?lua return billiards.tableheight + 0.08 ?>"
	    stroke="rgb(84, 139, 84)" stroke-width="0.008"
	    fill="rgb(107, 142, 35)" />

      <!-- The cushions. -->

      <use xlink:href="#side"/>
      <use transform="scale(-1, 1)" xlink:href="#side"/>
      <use transform="scale(-1, -1)" xlink:href="#side"/>
      <use transform="scale(1, -1)" xlink:href="#side"/>
      <use xlink:href="#top"/>
      <use transform="scale(-1, 1)" xlink:href="#top"/>

      <!-- The spots. -->

      <circle cx="<?lua return -0.25 * billiards.tablewidth ?>" cy="0"
	      r="0.01"
	      stroke="none"
	      fill="rgb(50, 70, 50)"/>

      <circle cx="<?lua return 0.25 * billiards.tablewidth ?>" cy="0"
	      r="0.01"
	      stroke="none"
	      fill="rgb(50, 70, 50)"/>

      <!-- The pockets. -->

      <use transform="translate(0 <?lua return -0.5 * billiards.tableheight -
		                                0.04 ?>)"
	   xlink:href="#pocket"/>

      <use transform="translate(0 <?lua return 0.5 * billiards.tableheight +
		                               0.04 ?>)
		      rotate(180)" xlink:href="#pocket"/>

      <use transform="translate(<?lua return 0.5 * billiards.tablewidth ?>
		                <?lua return -0.5 * billiards.tableheight ?>)
		      rotate(45)" xlink:href="#pocket"/>

      <use transform="translate(<?lua return 0.5 * billiards.tablewidth ?>
		                <?lua return 0.5 * billiards.tableheight ?>)
		      rotate(135)" xlink:href="#pocket"/>

      <use transform="translate(<?lua return -0.5 * billiards.tablewidth ?>
		                <?lua return -0.5 * billiards.tableheight ?>)
		      rotate(-45)" xlink:href="#pocket"/>

      <use transform="translate(<?lua return -0.5 * billiards.tablewidth ?>
		                <?lua return 0.5 * billiards.tableheight ?>)
		      rotate(-135)" xlink:href="#pocket"/>

      <?lua
	 local html = ""
	 local colors = {{255, 255, 255},
			 {204, 154, 5},
                         {34, 79, 165},
                         {202, 14, 14},
                         {40, 14, 94},
                         {204, 103, 2},
                         {13, 105, 70},
                         {116, 16, 30},
                         {0, 0, 0},
			 {204, 154, 5},
                         {34, 79, 165},
                         {202, 14, 14},
                         {40, 14, 94},
                         {204, 103, 2},
                         {13, 105, 70},
                         {116, 16, 30}}

	 -- Lay out the diamods.

	 for i = -3, 3 do
	    if i ~= 0 then
	       html = html .. string.format([[
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	              ]], 0.125 * i * billiards.tablewidth,
	                  0.5 * billiards.tableheight + 0.11,
  	                  0.125 * i * billiards.tablewidth,
	                  -0.5 * billiards.tableheight - 0.11)
	    end
	 end

	 for i = -1, 1 do
	    html = html .. string.format([[
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	           ]], 0.5 * billiards.tablewidth + 0.11,
	               0.25 * i * billiards.tableheight,
	               -0.5 * billiards.tablewidth - 0.11,
	               0.25 * i * billiards.tableheight)
	 end

	 if not query then
	    shot = {}

	    for _, ball in ipairs (bodies.balls) do
	       table.insert(shot, ball.trajectory)
	    end
	 else
	    shot = billiards.history[tonumber(query.shot)]
	 end

	 -- Draw the trajectories.

	 for i, line in ipairs(shot) do
	    html = html .. string.format([[
 	 <circle cx="%f" cy="%f" r="0.01" stroke="none"
		 fill="rgb(%d, %d, %d)"/>]], line[1][1],
                                             line[1][2],
	                                     colors[i][1],
                                             colors[i][2],
                                             colors[i][3])

	    html = html .. string.format([[
 	 <circle cx="%f" cy="%f" r="%f" fill="none"
		 stroke="rgb(%d, %d, %d)" stroke-width="0.003"/>]],
	                     line[1][1],
                             line[1][2],
	                     billiards.ballradius,
	                     colors[i][1],
                             colors[i][2],
                             colors[i][3])

	    html = html .. string.format([[

         <path fill="none" stroke="rgb(%d, %d, %d)" stroke-width="0.005"
	       d="M %.3f %.3f]], colors[i][1],
                                 colors[i][2],
                                 colors[i][3],
                                 line[1][1],
                                 line[1][2])

            for j = 1, #line do
	       html = html .. string.format([[

		  L %.3f %.3f]], line[j][1],
                                 line[j][2])
	    end      

	    html = html .. '"/>\n\n'
	 end

	 -- Draw the balls.

	 for i, line in ipairs(shot) do
	    html = html .. string.format ([[ 
         <g transform="translate(%f, %f)">]], line[#line][1], line[#line][2])

	    if line[#line][3] > 0 then
	       html = html .. string.format([[ 
	   <use fill="rgb(%d, %d, %d)" stroke="rgb(10, 10, 10)"
                xlink:href="#ball"/>]], colors[i][1],
	                                colors[i][2],
					colors[i][3])
	       if i > 8 then
		  html = html .. string.format([[ 
	   <use transform="rotate(%d)"
	        fill="url(#stripe)" xlink:href="#ball"/>]],
	  math.random (0, 180))
	       end
	    end
	    html = html .. [[
         </g>]]
	 end

	 return html
      ?>
  </g>
</svg>
