<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<head>

  <title>Game</title>
  <link rel="stylesheet" href="stylesheet">

    <style type="text/css">
      col.first {width : 12em;}
      col.second {width : 7em ;}
    </style>
</head>

<body>

  <hr> 
  <h1>
    <image type="image/svg+xml" src="/logo"/>
  </h1>
  
  <a href = "/">back</a>
  <hr> 

  <?lua
      if billiards.started then
	 return [[
    <form class="pseudoform" name="resumegame" action="/" method="post">
      <input type="hidden" name="resumegame" value="true" />
        <p>There's a game currently in progress.  If you've stopped it you can resume it <a href="javascript:document.resumegame.submit();">by clicking here</a>.</p>
    </form>
	    ]]
      end
   ?>

  <p>You can use the following links to start a new game.  Note that
    this only selects the proper table type and sets up the balls
    according to the specified game's opening positions. No rule checking
    or enforcement is implied.  Games which share the same opening setup
    (e.g. straight rail and cushion billiards) are therefore grouped
    together.
  </p>

  <p>Currently the following games are available:</p>

  <ul>
    <li> Pocket games
      <ul>
	<li>
	  <form class="pseudoform" name="eightballgame" action="/" method="post">
	    <input type="hidden" name="newgame" value="true" />
	    <input type="hidden" name="options.eightball" value="true" />
	    <input type="hidden" name="options.pool" value="true" />
	    <input type="hidden" name="options.billiards" value="false" />
	    <a href="javascript:document.eightballgame.submit();">
	      Eight-ball
	    </a>
	  </form>
	</li>
	<li>
	  <form class="pseudoform" name="nineballgame" action="/" method="post">
	    <input type="hidden" name="newgame" value="true" />
	    <input type="hidden" name="options.nineball" value="true" />
	    <input type="hidden" name="options.pool" value="true" />
	    <input type="hidden" name="options.billiards" value="false" />
	    <a href="javascript:document.nineballgame.submit();">
	      Nine-ball
	    </a>
	  </form>
	</li>
      </ul>
    <li> Pocketless games
      <ul>
	<li>
	  <form class="pseudoform" name="caromgame" action="/" method="post">
	    <input type="hidden" name="newgame" value="true" />
	    <input type="hidden" name="options.carom" value="true" />
	    <input type="hidden" name="options.pool" value="false" />
	    <input type="hidden" name="options.billiards" value="true" />
	    <a href="javascript:document.caromgame.submit();">
	      Billiards
	    </a>
	  </form>
	</li>
      </ul>
  </ul>

  <p>
    <form name="importgame" action="/" enctype="multipart/form-data" method="post">
      <input type="hidden" name="importgame" value="true" />
      You can also select a game that has been exported to a file: <input type="file" name="file" /> and <a href="javascript:document.importgame.submit();">import</a> it.
    </form>
  </p>

  </table>
  <hr> 
  
</body>
