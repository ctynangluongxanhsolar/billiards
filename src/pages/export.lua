<?lua
    -- Copyright (C) 2008 Papavasileiou Dimitris                             
    --                                                                      
    -- This program is free software: you can redistribute it and/or modify 
    -- it under the terms of the GNU General Public License as published by 
    -- the Free Software Foundation, either version 3 of the License, or    
    -- (at your option) any later version.                                  
    --                                                                      
    -- This program is distributed in the hope that it will be useful,      
    -- but WITHOUT ANY WARRANTY; without even the implied warranty of       
    -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    -- GNU General Public License for more details.                         
    --                                                                      
    -- You should have received a copy of the GNU General Public License    
    -- along with this program.  If not, see <http://www.gnu.org/licenses/>.

    local text = ""
  
    local function stringify (value)
       if type(value) == "string" then
	  return "\"" .. value .. "\""
       elseif type(value) == "table" then
	  local string = {}

	  for i, element in ipairs (value) do
	     string[i] = stringify(element) .. ","
	  end

	  for k, element in pairs (value) do
	     if type(k) == "string" then
		table.insert (string, k .. "=" .. stringify(element))
	     end
	  end
	  
	  return "{" .. table.concat(string) .. "}"
       else
	  return tostring(value)
       end
    end

    query.shot = tonumber (query.shot)

    for i, ball in ipairs (bodies.balls) do
       billiards.opening[i] = ball.position
    end

    for _, value in pairs {"billiards.tablewidth", 
			   "billiards.tableheight", 
			   "billiards.cushionheight", 
			   "billiards.ballradius", 
			   "billiards.ballmass", 
			   "billiards.cuemass", 
			   "billiards.cueinertia", 
			   "billiards.cueforce", 
			   "billiards.cuelength", 
			   "billiards.staticfriction", 
			   "billiards.slidingfriction", 
			   "billiards.rollingfriction", 
			   "billiards.spinningfriction", 
			   "billiards.strikingfriction", 
			   "billiards.slowfriction", 
			   "billiards.fastfriction", 
			   "billiards.bouncingfriction", 
			   "billiards.collidingrestitution", 
			   "billiards.strikingrestitution", 
			   "billiards.bouncingrestitution", 
			   "billiards.jumpingrestitution",
			   "options.pool",
			   "options.billiards",
			   "options.eightball",
			   "options.nineball",
			   "options.carom",
			   "billiards.opening"} do
       text = text .. string.format([[

%s = %s]], value, stringify (loadstring ("return " .. value)()))
    end
       
    text = text .. string.format([[

billiards.history = %s
				 ]],
				 stringify (query.shot and
					    {billiards.history[query.shot]} or
					    billiards.history))


    return text
?>
