/*
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

body {
	color: #000000 ;
	background-color: #FFFFFF ;
	font-family: sans-serif ;
	text-align: justify ;
	margin-right: 20px ;
	margin-left: 20px ;
}

a:link {
	color: #000080 ;
	background-color: inherit ;
	text-decoration: none ;
}

a:visited {
	background-color: inherit ;
	text-decoration: none ;
}

a:link:hover, a:visited:hover {
	color: #000080 ;
	background-color: #FCDF6D;
}

a:link:active, a:visited:active {
	color: #FF0000 ;
}

hr {
	border: 0 ;
	height: 1px ;
	color: #a0a0a0 ;
	background-color: #a0a0a0 ;
}

table {
   border-collapse: separate ;
   border-spacing: 0px 0px ;
   empty-cells: show
}

input.number {
   border-width: 1px;
   text-align : right ;
   background : #fafafa ;
}

input.text {
   border-width: 1 ;
   background : #fafafa ;
}

input.check {
   border-width: 1 ;
   background : #fafafa ;
}

input[type="file"] {
   background-color: #FCDF6D;
   border-width: 2px;
   border-style: dashed;
   border-color: orange;
}

tr.even {
   background : #FCDF6D ;
}

tr.odd {
   background : #DC5B3B ;
}

td {
   vertical-align :top ;
   border-top : solid ;
   border-bottom : solid ;
   border-width : thin ;
   border-color : #ebebeb ;
   padding-top : 2pt ;
   padding-bottom : 2pt
}

th {
   border-top : solid ;
   border-bottom : solid ;
   border-width : thin ;
   color : #3F1207 ;
   border-color : #3F1207
}

input[disabled], input[readonly], select[disabled], select[readonly], checkbox[disabled], checkbox[readonly], textarea[disabled], textarea[readonly] { 
    background-color: #dcdcdc; 
    color: #000000; 
}

form.pseudoform {
   display:inline;
}