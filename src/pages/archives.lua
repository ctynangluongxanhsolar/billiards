<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<?lua
    if query.restore then
       query.restore = nil
       
       for key, value in pairs (query) do
	  assert (loadstring(key .. "=" .. value))()
       end

       network.block = false
       graphics.hide = false

       resources.dofile "billiards/restart.lua"
    end
?>

<head>
  <title>Archives</title>
  <link rel="stylesheet" href="stylesheet">

  <style type="text/css">
    col.first {width : 12em;}
    col.second {width : 7em ;}
  </style>
</head>

<body>
  <hr> 
  <h1>
    <image type="image/svg+xml" src="/logo"/>
  </h1>
  
  <a href="/">back</a>
  <hr>

  <p> This is where all the games and shots you've archived so far end
    up.  Follow the links in the table below to restore or manage them.
  </p>

  <form action="/archives" method="post">
    <input type="hidden" id="action" name="action">
    <table width="100%">
      <col style="width:2em" />

      <tr><th colspan="3">Saved games and shots</th></tr>

      <?lua
        local html, games = "", {}

	-- Delete or rename selected games.

	if query.action then
	   if query.action == "delete" then
	      query.action = nil

	      for key in pairs (query) do
		 network.archive.pages[key] = nil
		 network.archive.tags[key] = nil
	      end
	   end

	   if query.action == "rename" then
	      query.action = nil

	      for key, value in pairs (query) do
		 network.archive.tags[key] = value[2]
	      end
	   end	   
	end

        -- TODO: fix this when Lua 5.2 arrives.

        for uri, tag in getmetatable(network.archive.tags).__pairs() do
	   table.insert (games, {uri, tag})
	end
      
	table.sort (games, function (a, b) return a[1] > b[1] end)

	for i, game in ipairs (games) do
	   html = html .. string.format([[

      <tr class="%s">
        <td class="striped">
          <input type="checkbox" name="%s" value="true"
	         onchange="updateDescription(this);"/>
        </td>
        <td class="striped" align="left">
          <a href="%s">%s</a>
          <input type="text" name="%s" value="%s" disabled="true"
		 style="width: 100%%; display: none"/>
        </td>
      </tr>
				]], math.mod(i, 2) == 1 and "even" or "odd",
			            game[1],
				    game[1], game[2],
				    game[1], game[2])
      end

      return html
    ?>
    </table>
    <a href="javascript:submit('delete');">delete</a>
    &middot;
    <a href="javascript:submit('rename');">rename</a>
  </form>

  <script type="text/javascript">
       function submit(action)
       {
	  document.getElementById("action").value = action;
	  document.forms[0].submit();
       }

       function updateDescription(check)
       {
	  var row = check.parentNode.parentNode;
	  var input = row.getElementsByTagName("input")[1];
	  var link = row.getElementsByTagName("a")[0];

	  if (check.checked) {
	     input.style.display = "inline";
	     input.disabled = false;

	     link.style.display = "none";
	     link.disabled = true;
	  } else {
	     input.style.display = "none";
	     input.disabled = true;

	     link.style.display = "inline";	     
	     link.disabled = false;
	  }
       }
  </script>
</body>  
