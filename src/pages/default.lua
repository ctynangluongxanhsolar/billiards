<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
  
<?lua
 if query.resumegame then
    network.block = false
    graphics.hide = false
 elseif query.newgame then
    billiards.history = {}

    billiards.opening = nil
    billiards.decals = nil

    options.eightball = false
    options.nineball = false
    options.carom = false

    options.pool = false
    options.billiards = false

    for key, value in pairs (query) do
       assert (loadstring(key .. "=" .. value))()
    end

    network.block = false
    graphics.hide = false

    resources.dofile "billiards/restart.lua"
 elseif query.importgame and #query.file[3] > 0 then
    restoregame = loadstring(query.file[3])

    if restoregame then
       restoregame()

       network.block = false
       graphics.hide = false

       resources.dofile "billiards/restart.lua"
    end
 end
 ?>

<head>
  <title>Billiards</title>
  <link rel="stylesheet" href="stylesheet" type="text/css">
</head>

<body>
  <hr> 

  <h1>
    <image type="image/svg+xml" src="/logo"/>
  </h1>

  <a href="/newgame">game</a>
  &middot;
  <?lua
    if billiards.started then
       return [[
  <a href="/history">shots</a>
  &middot;
       ]]
    end
  ?>
  <a href="/archives">archives</a>
  &middot;
  <a href="/settings">settings</a>
  &middot;
  <form class="pseudoform" style="display:inline;" name="quit" action="/setvalue" method="post">
    <input type="hidden" name="common.iterate" value="false" />
    <input type="hidden" name="network.block" value="false" />
    <a href="javascript:document.quit.submit();">quit</a>
  </form>
  <hr> 

  <p>
    Hello and welcome to Billiards, the free billiards simulator. Use
    the links at the top to start a new game, review your progress so
    far or change physical parameters and system options like window
    size etc.
  </p>

  <p>
    Playing billiards is simple enough.  You don't even need your
    keyboard.  Point your mouse at anything but the cue and balls and
    drag using the left mouse button to look around.  The right mouse
    button zooms.
  </p>

  <p>
    Pointing at a ball and clicking the left mouse button either
    selects this ball as the cueball (you can shoot any ball, not just
    the white ones) or lines up a shot, if the selected ball already
    is the cueball.  Selecting a ball with the right mouse button
    allows you to drag that ball around.
  </p>

  <p>
    The cue is divided into two parts: the tip and the rest.
    Selecting and dragging the tip (with the left mouse button)
    applies english to the shot.  Dragging the rest of the cue with
    the left mouse button swings the cue and, with some luck, shoots
    the ball in the desired direction.  Dragging with the right mouse
    button will prove useful to the showoffs among you as it adjusts
    cue elevation allowing you to perform mass&eacute; shots and the
    like.
  </p>

  <hr>      
</body>
