<?xml version="1.0" standalone="no"?>

<!--
    Copyright (C) 2008 Papavasileiou Dimitris                             
                                                                     
    This program is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation, either version 3 of the License, or    
    (at your option) any later version.                                  
    
    This program is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of       
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
    GNU General Public License for more details.                         
                                                                     
    You should have received a copy of the GNU General Public License    
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" 
	    "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

  <svg <?lua
           return query.width and string.format([[
       width="%dpx"
       height="%dpx"
				  ]],
				  query.width,
				  query.width *
				     (billiards.tableheight + 0.4) /
				     (billiards.tablewidth + 0.4))
	?>
       viewBox="<?lua
		  return string.format("%f %f %f %f",
		                       -0.5 * billiards.tablewidth - 0.2,
		                       -0.5 * billiards.tableheight - 0.2,
		                       billiards.tablewidth + 0.4,
		                       billiards.tableheight + 0.4)
		?>"
       xmlns="http://www.w3.org/2000/svg" version="1.1" 
       xmlns:xlink="http://www.w3.org/1999/xlink">

    <defs>
      <g id="diamond" transform="translate(0 -0.0141) rotate(45)">
	<rect width="0.02" height="0.02" fill="white" stroke="none"/>
      </g>

      <g id="ball">
	<circle r="<?lua return billiards.ballradius ?>" stroke-width="0.003"/>
      </g>
    </defs>

    <g transform="scale(-1, 1)" id="table">
      <!-- The frame. -->

      <rect x="<?lua return -0.5 * billiards.tablewidth - 0.19 ?>"
	    y="<?lua return -0.5 * billiards.tableheight - 0.19 ?>"
	    rx="0.09"
            width="<?lua return billiards.tablewidth + 0.38 ?>"
	    height="<?lua return billiards.tableheight + 0.38 ?>"
	    fill="rgb(120, 80, 50)" 
  	    stroke="rgb(225, 185, 135)" stroke-width="0.015"/>

      <rect x="<?lua return -0.5 * billiards.tablewidth - 0.04 ?>"
	    y="<?lua return -0.5 * billiards.tableheight - 0.04 ?>"
	    width="<?lua return billiards.tablewidth + 0.08 ?>"
            height="<?lua return billiards.tableheight + 0.08 ?>"
	    stroke="rgb(50, 50, 50)" stroke-width="0.003"
  	    fill="rgb(85, 130, 160)" />

      <rect x="<?lua return -0.5 * billiards.tablewidth ?>"
	    y="<?lua return -0.5 * billiards.tableheight ?>"
	    width="<?lua return billiards.tablewidth ?>"
            height="<?lua return billiards.tableheight ?>"
	    stroke="rgb(70, 90, 125)" stroke-width="0.008"
            fill="rgb(125, 165, 200)" />

      <!-- The spots. -->

      <circle cx="<?lua return -0.25 * billiards.tablewidth ?>" cy="0"
	      r="0.01"
	      stroke="none"
	      fill="rgb(85, 130, 160)"/>

      <circle cx="<?lua return 0.25 * billiards.tablewidth ?>" cy="0"
	      r="0.01"
	      stroke="none"
	      fill="rgb(85, 130, 160)"/>
      
      <circle cx="<?lua return 0.25 * billiards.tablewidth ?>" cy="0.1524"
	      r="0.01"
  	      stroke="none"
	      fill="rgb(85, 130, 160)"/>

      <?lua
	 local html = ""
	 local colors = {{247, 247, 209},
			 {235, 168, 46},
	                 {102, 31, 26}}

	 -- Lay out the diamonds.

	 for i = -3, 3 do
	    html = html .. string.format([[
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	           ]], 0.125 * i * billiards.tablewidth,
	               0.5 * billiards.tableheight + 0.11,
  	               0.125 * i * billiards.tablewidth,
	               -0.5 * billiards.tableheight - 0.11)
	 end

	 for i = -1, 1 do
	    html = html .. string.format([[
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	 <use x="%f" y="%f" xlink:href="#diamond"/>
	           ]], 0.5 * billiards.tablewidth + 0.11,
	               0.25 * i * billiards.tableheight,
	               -0.5 * billiards.tablewidth - 0.11,
	               0.25 * i * billiards.tableheight)
	 end

	 if not query then
	    shot = {}

	    for _, ball in ipairs (bodies.balls) do
	       table.insert(shot, ball.trajectory)
	    end
	 else
	    shot = billiards.history[tonumber(query.shot)]
	 end

	 -- Draw the trajectories.

	 for i, line in ipairs(shot) do
	    html = html .. string.format([[
 	 <circle cx="%f" cy="%f" r="0.01" stroke="none"
		 fill="rgb(%d, %d, %d)"/>]], line[1][1],
                                             line[1][2],
	                                     colors[i][1],
                                             colors[i][2],
                                             colors[i][3])

	    html = html .. string.format([[
 	 <circle cx="%f" cy="%f" r="%f" fill="none"
		 stroke="rgb(%d, %d, %d)" stroke-width="0.003"/>]],
	                     line[1][1],
                             line[1][2],
	                     billiards.ballradius,
	                     colors[i][1],
                             colors[i][2],
                             colors[i][3])

	    html = html .. string.format([[

         <path fill="none" stroke="rgb(%d, %d, %d)" stroke-width="0.005"
	       d="M %.3f %.3f]], colors[i][1],
                                 colors[i][2],
                                 colors[i][3],
                                 line[1][1],
                                 line[1][2])

            for j = 1, #line do
	       html = html .. string.format([[

		  L %.3f %.3f]], line[j][1],
                                 line[j][2])
	    end      

	    html = html .. '"/>\n\n'
	 end

	 -- Draw the balls.

	 for i, line in ipairs(shot) do
	    html = html .. string.format([[
	 <use x="%f" y="%f" fill="rgb(%d, %d, %d)"
              stroke="rgb(10, 10, 10)" xlink:href="#ball"/>
	              ]], line[#line][1],
                          line[#line][2],
                          colors[i][1],
                          colors[i][2],
                          colors[i][3])
	    if i > 8 then
	       html = html .. string.format([[
	 <use transform="translate(%f, %f) rotate(%d)"
	      fill="url(#stripe)" xlink:href="#ball"/>
	              ]], line[#line][1],
		          line[#line][2],
		          math.random (0, 180))
	    end
	 end

	 return html
      ?>
  </g>
</svg>
