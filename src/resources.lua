-- Copyright (C) 2008 Papavasileiou Dimitris                             
--                                                                      
-- This program is free software: you can redistribute it and/or modify 
-- it under the terms of the GNU General Public License as published by 
-- the Free Software Foundation, either version 3 of the License, or    
-- (at your option) any later version.                                  
--                                                                      
-- This program is distributed in the hope that it will be useful,      
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        
-- GNU General Public License for more details.                         
--                                                                      
-- You should have received a copy of the GNU General Public License    
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "meshes"
require "textures"
require "dynamics"

-- This scales the table mesh (and possibly related meshes
-- such as panels etc.) based on a desired table width/height.

local function scale(mesh)
   local n = mesh.size[1]
   -- TODO: Fix this with Lua 5.2 arrives.
   local k = mesh.vertices[8 * n] and 8 or 3-- #mesh.vertices / mesh.size[1]
   local dx = 0.5 * (billiards.tablewidth - 2.84)
   local dy = 0.5 * (billiards.tableheight - 1.42)

   for i = 0, n - 1 do
      if mesh.vertices[k * i + 1] > 0.2 then
	 mesh.vertices[k * i + 1] = mesh.vertices[k * i + 1] + dx
      elseif mesh.vertices[k * i + 1] < -0.2 then
	 mesh.vertices[k * i + 1] = mesh.vertices[k * i + 1] - dx
      end

      if mesh.vertices[k * i + 2] > 0 then
	 mesh.vertices[k * i + 2] = mesh.vertices[k * i + 2] + dy
      elseif mesh.vertices[k * i + 2] < 0 then
	 mesh.vertices[k * i + 2] = mesh.vertices[k * i + 2] - dy
      end
   end

   return mesh
end

-- The following two function take a single diamond mesh and
-- return a new one which has multiple copies of the diamond
-- laid out properly.

local function addcopy(from, to, deltax, deltay)
   local n = from.size[1]
   local m = from.size[2]

   for j = 0, n - 1 do
      table.insert(to.vertices, from.vertices[j * 8 + 1] + deltax)
      table.insert(to.vertices, from.vertices[j * 8 + 2] + deltay)
      table.insert(to.vertices, from.vertices[j * 8 + 3])

      table.insert(to.vertices, from.vertices[j * 8 + 4])
      table.insert(to.vertices, from.vertices[j * 8 + 5])
      table.insert(to.vertices, from.vertices[j * 8 + 6])

      table.insert(to.vertices, from.vertices[j * 8 + 7])
      table.insert(to.vertices, from.vertices[j * 8 + 8])
   end

   for j = 1, m do
      table.insert(to.indices, from.indices[j] + to.size[1])
   end

   to.size = {#to.vertices / 8, #to.indices}
end

local function layout(mesh)
   local dx = 0.5 * billiards.tablewidth
   local dy = 0.5 * billiards.tableheight
   local new = {
      size = {0, 0},
      vertices = {},
      indices = {}
   }
   
   for i = -3, 3 do
      if i ~= 0 or options.billiards then
	 addcopy (mesh, new, i * dx / 4, -(dy + 0.1))
	 addcopy (mesh, new, i * dx / 4, dy + 0.1)
       end
   end
   
   for i = -1, 1 do
      addcopy (mesh, new, -(dx + 0.1), i * dy / 2)
      addcopy (mesh, new, dx + 0.1, i * dy / 2)
   end

   return new
end

local function patched (texture)
   texture.levels = {0, 5}

   return texture
end

resources.patched = resources.pipeline {resources.dofile,
					patched,
					textures.clamped}

resources.scaled = resources.uncached {resources.dofile,
				       scale,
				       meshes.static}

resources.laidout = resources.uncached {resources.dofile,
					layout,
					meshes.static}

resources.polyhedron = resources.uncached {resources.dofile,
					   scale,
					   bodies.polyhedron}
